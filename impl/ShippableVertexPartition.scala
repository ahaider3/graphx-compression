/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.spark.graphx.impl

// import scala.reflect.ClassTag
import scala.reflect.{ClassTag, classTag}
import org.apache.spark.util.collection.{BitSet, PrimitiveVector}
import java.io.ByteArrayOutputStream
import scala.language.postfixOps
import java.io.ByteArrayInputStream
import java.io.DataOutputStream
import java.io.DataInputStream
import java.nio.ByteBuffer
import java.io.ObjectOutputStream
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream
import scala.reflect.runtime.universe._
import org.apache.spark.graphx._
import org.apache.spark.graphx.util.collection.GraphXPrimitiveKeyOpenHashMap
// scalastyle:off println
/** Stores vertex attributes to ship to an edge partition. */
private[graphx]
class VertexAttributeBlock[VD: ClassTag](val vids: Array[VertexId], val vidsComp: Array[Byte],
 val attrs: Array[VD], val attrsComp: Array[Byte])
  extends Serializable {
  def iterator: Iterator[(VertexId, VD)] =
    (0 until vids.size).iterator.map { i => (vids(i), attrs(i)) }
}

private[graphx]
object ShippableVertexPartition {
  /** Construct a `ShippableVertexPartition` from the given vertices without any routing table. */
  def apply[VD: ClassTag](iter: Iterator[(VertexId, VD)]): ShippableVertexPartition[VD] =
    apply(iter, RoutingTablePartition.empty, null.asInstanceOf[VD], (a, b) => a)

  /**
   * Construct a `ShippableVertexPartition` from the given vertices with the specified routing
   * table, filling in missing vertices mentioned in the routing table using `defaultVal`.
   */
  def apply[VD: ClassTag](
      iter: Iterator[(VertexId, VD)], routingTable: RoutingTablePartition, defaultVal: VD)
    : ShippableVertexPartition[VD] =
    apply(iter, routingTable, defaultVal, (a, b) => a)
  def compress[VD: ClassTag](
      vals: Array[VD]): Array[Byte] = {
      var z = -1
      println("FINDING")
      vals match {
 case intArr: Array[Int @unchecked] if classTag[VD] == classTag[Int] => z = 0
 case douArr: Array[Double @unchecked] if classTag[VD] == classTag[Double] => z = 1
      }
    var bb = ByteBuffer.allocate(vals.size * 8)
    var i = 0
    if ( z == 0){
      println("INTEG")
      while( i < vals.size) {
          bb.putInt(vals(i).asInstanceOf[Integer]);
          i += 1
      }
    }
    else if (z == 1){
      i = 0
      println("DOU")
      while ( i < vals.size ){
        bb.putDouble(vals(i).asInstanceOf[Double])
        i += 1
      }
    }
    else {
     println("NOTINTORDOU")
    }
    var buf = new ByteArrayOutputStream()
   val compressor = new BZip2CompressorOutputStream(buf)
   val arr = bb.array()
   compressor.write(arr, 0, arr.size)
   compressor.flush()
   compressor.close()
   buf.toByteArray()

 }

  /**
   * Construct a `ShippableVertexPartition` from the given vertices with the specified routing
   * table, filling in missing vertices mentioned in the routing table using `defaultVal`,
   * and merging duplicate vertex atrribute with mergeFunc.
   */
  def apply[VD: ClassTag](
      iter: Iterator[(VertexId, VD)], routingTable: RoutingTablePartition, defaultVal: VD,
      mergeFunc: (VD, VD) => VD): ShippableVertexPartition[VD] = {
    val map = new GraphXPrimitiveKeyOpenHashMap[VertexId, VD]
    // Merge the given vertices using mergeFunc
    iter.foreach { pair =>
      map.setMerge(pair._1, pair._2, mergeFunc)
    }
    // Fill in missing vertices mentioned in the routing table
    routingTable.iterator.foreach { vid =>
      map.changeValue(vid, defaultVal, identity)
    }
    // val arr = compress(map._values)
    val temp = map._values
    var i = 0
    val values1 = new Array[Any](temp.size)
    while ( i < temp.size ) {
       values1(i) = temp(i)
       i += 1
    }
new ShippableVertexPartition(map.keySet, map._values, map.keySet.getBitSet,
 routingTable)
  }

  import scala.language.implicitConversions

  /**
   * Implicit conversion to allow invoking `VertexPartitionBase` operations directly on a
   * `ShippableVertexPartition`.
   */
  implicit def shippablePartitionToOps[VD: ClassTag](partition: ShippableVertexPartition[VD])
    : ShippableVertexPartitionOps[VD] = new ShippableVertexPartitionOps(partition)

  /**
   * Implicit evidence that `ShippableVertexPartition` is a member of the
   * `VertexPartitionBaseOpsConstructor` typeclass. This enables invoking `VertexPartitionBase`
   * operations on a `ShippableVertexPartition` via an evidence parameter, as in
   * [[VertexPartitionBaseOps]].
   */
  implicit object ShippableVertexPartitionOpsConstructor
    extends VertexPartitionBaseOpsConstructor[ShippableVertexPartition] {
    def toOps[VD: ClassTag](partition: ShippableVertexPartition[VD])
      : VertexPartitionBaseOps[VD, ShippableVertexPartition] = shippablePartitionToOps(partition)
  }
}

/**
 * A map from vertex id to vertex attribute that additionally stores edge partition join sites for
 * each vertex attribute, enabling joining with an [[org.apache.spark.graphx.EdgeRDD]].
 */
private[graphx]
class ShippableVertexPartition[VD: ClassTag](
    val index: VertexIdToIndexMap,
    val values: Array[VD],
    val mask: BitSet,
    val routingTable: RoutingTablePartition)
  extends VertexPartitionBase[VD] {

  /** Return a new ShippableVertexPartition with the specified routing table. */
  def withRoutingTable(routingTable_ : RoutingTablePartition): ShippableVertexPartition[VD] = {
    new ShippableVertexPartition(index, values, mask, routingTable_)
  }

  /**
   * Generate a `VertexAttributeBlock` for each edge partition keyed on the edge partition ID. The
   * `VertexAttributeBlock` contains the vertex attributes from the current partition that are
   * referenced in the specified positions in the edge partition.
   */
  def shipVertexAttributes(
      shipSrc: Boolean, shipDst: Boolean): Iterator[(PartitionID, VertexAttributeBlock[VD])] = {
    println("SHIPATTR")
      val t1 = System.nanoTime()
      val vertDelta = routingTable.getVertDelta()
      val bitsets = routingTable.getBitSets()
      var j = 0
      var currSum = 0
      while ( j < vertDelta.size) {
//             currSum += vertDelta(j)
             currSum += 1
             var currPid = 0
             while (currPid < bitsets.size){
                      if (bitsets(currPid).get(0)){
                               // add this to pid vab pair
                               // attrs += vertArr(j)
                      }
                      currPid += 1
             }
             j += 1
      }
      val time1 = System.nanoTime() - t1
      println(s"SHIPATTRTIME: $time1")
    Iterator.tabulate(routingTable.numEdgePartitions) { pid =>
      val initialSize = if (shipSrc && shipDst) routingTable.partitionSize(pid) else 64
      val vids = new PrimitiveVector[VertexId](initialSize)
      val attrs = new PrimitiveVector[VD](initialSize)
      var i = 0
      val t2 = System.nanoTime()
      routingTable.foreachWithinEdgePartition(pid, shipSrc, shipDst) { vid =>
        if (isDefined(vid)) {
          vids += vid
          attrs += this(vid)
          val temp = this(vid)
        //  println(s"TESTING $temp")
        }
        i += 1
      }
      val time2 = System.nanoTime() - t2
      println(s"SHIPATTRTIMEORIG: $time2")
      // val arrIds = compressIds(vids.trim().array)
      // val arrAttrs = compressAtr(attrs.trim().array)
      (pid, new VertexAttributeBlock(vids.trim().array, null, attrs.trim().array, null))
    }
  }
  def compressAtr[VD: ClassTag](
      vals: Array[VD]): Array[Byte] = {
      var z = -1
      println("FINDING1")
 //     vals match {
 // case intArr: Array[Int @unchecked] if classTag[VD] == classTag[Int] => z = 0
 // case douArr: Array[Double @unchecked] if classTag[VD] == classTag[Double] => z = 1
  //    }
    var i = 0
    var bb = ByteBuffer.allocate(vals.size * 8)
    while( i < vals.size){
    vals(i) match {
    case intVal: Int if classTag[VD] == classTag[Int] => {
          val temp: Double = vals(i).asInstanceOf[Integer] * 1.0
          bb.putDouble(temp)
    }
    case douVal: Double if classTag[VD] == classTag[Double] => {
          bb.putDouble(vals(i).asInstanceOf[Double]);
    }
    case _ => {
  if ( vals(i) != null ) {
  val temp = ((vals(i)).asInstanceOf[Tuple2[Double, Double]])
  val temp1 = ((temp)_2).asInstanceOf[Double]
          bb.putDouble(temp1)
     }
    }
   }
   i += 1
  }
   var buf = new ByteArrayOutputStream()
   val compressor = new BZip2CompressorOutputStream(buf)
   val arr = bb.array()
   compressor.write(arr, 0, arr.size)
   compressor.flush()
   compressor.close()
   buf.toByteArray()
 }
  def compressIds(
      vals: Array[VertexId]): Array[Byte] = {
      var bb = ByteBuffer.allocate(vals.size * 8)
      var i = 0
      while ( i < vals.size ){
        bb.putLong(vals(i).asInstanceOf[Long])
        i += 1
      }
   var buf = new ByteArrayOutputStream()
   val compressor = new BZip2CompressorOutputStream(buf)
   val arr = bb.array()
   compressor.write(arr, 0, arr.size)
   compressor.flush()
   compressor.close()
   buf.toByteArray()
 }

  /**
   * Generate a `VertexId` array for each edge partition keyed on the edge partition ID. The array
   * contains the visible vertex ids from the current partition that are referenced in the edge
   * partition.
   */
  def shipVertexIds(): Iterator[(PartitionID, Array[VertexId])] = {
    println("SHIPIDS")
      val vertDelta = routingTable.getVertDelta()
      val bitsets = routingTable.getBitSets()
      var j = 0
      var currSum = 0
      while ( j < vertDelta.size) {
 //            currSum += vertDelta(j)
             currSum += 1
             var currPid = 0
             while (currPid < bitsets.size){
                      if (bitsets(currPid).get(0)){
                               // add this to pid vab pair
                      }
                      currPid += 1
             }
             j += 1
      }

    Iterator.tabulate(routingTable.numEdgePartitions) { pid =>
      val vids = new PrimitiveVector[VertexId](routingTable.partitionSize(pid))
      var i = 0
      routingTable.foreachWithinEdgePartition(pid, true, true) { vid =>
        if (isDefined(vid)) {
          vids += vid
        }
        i += 1
      }
      // val arr = compressIds(vids.trim().array)
      (pid, vids.trim().array)
    }
  }
}

private[graphx] class ShippableVertexPartitionOps[VD: ClassTag](self: ShippableVertexPartition[VD])
  extends VertexPartitionBaseOps[VD, ShippableVertexPartition](self) {

  def withIndex(index: VertexIdToIndexMap): ShippableVertexPartition[VD] = {
    new ShippableVertexPartition(index, self.values, self.mask, self.routingTable)
  }
  def withValues[VD2: ClassTag](values: Array[VD2]): ShippableVertexPartition[VD2] = {
    new ShippableVertexPartition(self.index, values, self.mask, self.routingTable)
  }

  def withMask(mask: BitSet): ShippableVertexPartition[VD] = {
    new ShippableVertexPartition(self.index, self.values, mask, self.routingTable)
  }
}
// scalastyle:on println
