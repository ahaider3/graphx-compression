/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.spark.graphx.impl
import scala.reflect.{ClassTag, classTag}
import org.xerial.snappy.{Snappy, SnappyInputStream, SnappyOutputStream}
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream
import java.io.ByteArrayOutputStream
import java.io.ByteArrayInputStream
import java.io.DataOutputStream
import java.io.DataInputStream
import java.nio.ByteBuffer
import java.io.ObjectOutputStream
import org.apache.spark.util.collection.BitSet
import org.apache.spark.graphx._
import org.apache.spark.graphx.util.collection.GraphXPrimitiveKeyOpenHashMap
import org.apache.spark.util.collection.{SortDataFormat, Sorter, PrimitiveVector}
// scalastyle:off println
/** Constructs an EdgePartition from scratch. */
private[graphx]
class EdgePartitionBuilder[@specialized(Long, Int, Double) ED: ClassTag, VD: ClassTag](
    size: Int = 64) {
  private[this] val edges = new PrimitiveVector[Edge[ED]](size)

  /** Add a new edge to the partition. */
  def add(src: VertexId, dst: VertexId, d: ED) {
    edges += Edge(src, dst, d)
  }

  def toEdgePartition: EdgePartition[ED, VD] = {
    val edgeArray = edges.trim().array
    new Sorter(Edge.edgeArraySortDataFormat[ED])
      .sort(edgeArray, 0, edgeArray.length, Edge.lexicographicOrdering)
    val localSrcIds = new Array[Int](edgeArray.size)
    val localDstIds = new Array[Int](edgeArray.size)
    val data = new Array[ED](edgeArray.size)
    val index1 = new GraphXPrimitiveKeyOpenHashMap[VertexId, Int]
    var index = new Array[VertexId](edgeArray.size)
    var indexPair = new Array[Int](edgeArray.size)
    val global2local = new GraphXPrimitiveKeyOpenHashMap[VertexId, Int]
    val global2local1 = new GraphXPrimitiveKeyOpenHashMap[VertexId, Int]

    val local2global = new PrimitiveVector[VertexId]
    var vertexAttrs = Array.empty[VD]
    var temp9 = 0
    var curr = 1
    index(0) = edgeArray(0).srcId
    indexPair(0) = 0
    var capture1 = 0
    var capture = 0
    var allVerts = new Array[Long](edgeArray.size)
    var currLocalId = -1
    var currLocalId1 = -1
 /**   if (edgeArray.length > 0){
         var i = 0
         while ( i < edgeArray.size) {
         val srcId = edgeArray(i).srcId
        if ( global2local.getOrElse(srcId, -1) == -1 ){
         allVerts(capture1) = srcId.asInstanceOf[Long]
         capture1 += 1
         }
         global2local.changeValue(srcId,
           { currLocalId += 1; local2global += srcId; currLocalId}, identity)
         i += 1
         }
    }   */
    // Copy edges into columnar structures, tracking the beginnings of source vertex id clusters and
    // adding them to the index. Also populate a map from vertex id to a sequential local offset.
    println("DONE")
    if (edgeArray.length > 0) {
      index1.update(edgeArray(0).srcId, 0)
      var currSrcId: VertexId = edgeArray(0).srcId
 //     var currLocalId = -1
      var i = 0
      while (i < edgeArray.size) {
        val srcId = edgeArray(i).srcId
        val dstId = edgeArray(i).dstId
        var flag = true
        var flag1 = true
        var capture2 = 0
        if ( global2local1.getOrElse(srcId, -1) == -1 ){
         allVerts(capture1) = srcId.asInstanceOf[Long]
         capture1 += 1
         }
        if ( global2local1.getOrElse(dstId, -1) == -1 ){
         allVerts(capture1) = dstId.asInstanceOf[Long]
         capture1 += 1
         }
        localSrcIds(i) = global2local1.changeValue(srcId,
          { currLocalId1 += 1; local2global += srcId; currLocalId1 }, identity)
        var loc = localSrcIds(i)
        localDstIds(i) = global2local1.changeValue(dstId,
          { currLocalId1 += 1; local2global += dstId; currLocalId1 }, identity)
        var loc1 = localDstIds(i)
//        println(s"SRC: $loc DST: $loc1 GLOSRC: $srcId GLODST: $dstId")
        data(i) = edgeArray(i).attr
        if (srcId != currSrcId) {
          currSrcId = srcId
       index1.update(currSrcId, i)
          index(curr) = currSrcId
          indexPair(curr) = i
          curr += 1
        }

        i += 1
      }
      vertexAttrs = new Array[VD](currLocalId1 + 1)
      temp9 = currLocalId1 + 1
      capture = currLocalId1 + 1
    }
    println("DONE1")
    var loCount1 = 0
    var gloCount1 = 0
    var gloCount = 0
    var loCount = 0
    var common = 0
    var tempDstLocal = new Array[Int](1000000)
    var tempGlobal = new Array[Long](1000000)

    // DELTA ENCODING of localSrc
    var loc = 0
    var prev = localSrcIds(loc)
    loc += 1
    var deltaSrc = new BitSet(localSrcIds.size)
    while ( loc < localSrcIds.size){
         if ( localSrcIds(loc) != prev){
              deltaSrc.set(loc)
               prev = localSrcIds(loc)
     /**          val tempDst1 = new Array[Int](common)
               val tempGlo1 = new Array[Long](common)
               System.arraycopy(tempGlobal, 0, tempGlo1, 0, common)
               System.arraycopy(tempDstLocal, 0, tempDst1, 0, common)
               scala.util.Sorting.quickSort(tempGlo1)
               scala.util.Sorting.quickSort(tempDst1)
               var j = 1
               while ( j < tempDst1.size){
                val dif = tempDst1(j) - tempDst1(j-1)
                if (dif > 127 || dif < -128){
                       loCount += 1
               }
                else {
                       loCount1 += 1
               }
                 j += 1
              }
               j = 1
               while ( j < tempGlo1.size){
                val dif = tempGlo1(j) - tempGlo1(j-1)
                if (dif > 127 || dif < -128){
                       gloCount += 1
               }
                else {
                       gloCount1 += 1
               }
               j += 1
             }
            tempDstLocal = new Array[Int](100000)
            tempGlobal = new Array[Long](100000)
            common = 0 */
           }
     /**    else {
         tempGlobal(common) = local2global(localDstIds(loc)).asInstanceOf[Long]
         tempDstLocal(common) = localDstIds(loc)
         common += 1
         }  */
         loc += 1
    }
    val numEdges = localSrcIds.size
    println(s"SRCBITS: $numEdges")
  //  println(s"COMPARE SIZE INDEX: $deltaOrigCurr BYTE: $deltaCurr")
    var loc1 = 0
    var deltaSrc1 = new Array[Byte](localDstIds.size)
    var deltaOrig1 = new Array[Int](localDstIds.size)
    var deltaCurr1 = 0
    var deltaOrigCurr1 = 0
    deltaOrig1(deltaCurr1) = localDstIds(loc1)
    loc1 += 1
    deltaOrigCurr1 += 1
    var sum1 = localSrcIds(0)
    while ( loc1 < localDstIds.size){
       val dif = localDstIds(loc1) - sum1
       if (dif > 127 || dif < -128){
            deltaOrig1(deltaOrigCurr1) = localDstIds(loc1)
            deltaOrigCurr1 += 1
            sum1 = localDstIds(loc1)
       }
       else {
         deltaSrc1(deltaCurr1) = dif.asInstanceOf[Byte]
         sum1 += dif
         deltaCurr1 += 1
       }
       loc1 += 1
    }
    var RdeltaSrc1 = new Array[Byte](deltaCurr1)
    System.arraycopy(deltaSrc1, 0, RdeltaSrc1, 0, deltaCurr1)
    var RdeltaOrig1 = new Array[Int](deltaOrigCurr1)
    System.arraycopy(deltaOrig1, 0, RdeltaOrig1, 0, deltaOrigCurr1)
    val auxil_inverse = new Array[Int](capture1)
    val deltaDstSize = deltaOrigCurr1 * 4
    println(s"COMPARE SIZE DST INDEX: $deltaDstSize BYTE: $deltaCurr1")
 //   println(s"DELTAGLO: $gloCount1 GLO: $gloCount")
 //   println(s"DELTADST: $loCount1 DST: $loCount")
    var loc9 = 0
    var arrIndex = new Array[Long](capture1)
    while ( loc9 < capture1 ){
          arrIndex(loc9) = allVerts(loc9)
          loc9 += 1
    }
    allVerts = arrIndex
    scala.util.Sorting.quickSort(allVerts)
    var auxil = new Array[Int](capture1)
    var loc2 = 0
    while ( loc2 < capture1 ){
        val ind = global2local1.getOrElse(allVerts(loc2), -1)
        if ( ind != -1) {
        auxil(ind) = loc2
        auxil_inverse(loc2) = ind
        }
        loc2 += 1
    }
    loc2 = 0
    var deltaIndex2 = new Array[Byte](capture1)
    var deltaOrig2 = new Array[VertexId](capture1)
    var deltaCurr2 = 0
    var deltaOrigCurr2 = 0
    deltaOrig2(deltaCurr2) = allVerts(loc2)
    loc2 += 1
    deltaOrigCurr2 += 1
    var sum2 = allVerts(0)
    var p = 0
    while ( loc2 < capture1){
       val dif = allVerts(loc2) - sum2
       if (dif > 127 || dif < -128 || p == 4){
            deltaOrig2(deltaOrigCurr2) = allVerts(loc2)
            deltaOrigCurr2 += 1
            sum2 = allVerts(loc2)
             p = 0
       }
       else {
         deltaIndex2(deltaCurr2) = dif.asInstanceOf[Byte]
         sum2 += dif
         deltaCurr2 += 1
         p += 1
       }
       loc2 += 1
    }
    var RdeltaOrig2 = new Array[VertexId](deltaOrigCurr2)
    System.arraycopy(deltaOrig2, 0, RdeltaOrig2, 0, deltaOrigCurr2)
    var RdeltaIndex2 = new Array[Byte](deltaCurr2)
    System.arraycopy(deltaIndex2, 0, RdeltaIndex2, 0, deltaCurr2)

    var Rauxil = new Array[Int](capture1)
    System.arraycopy(auxil, 0, Rauxil, 0, capture1)
    val runSumSize = deltaOrigCurr2 * 8
println(s"COMPARESIZEINDINDEX $runSumSize B $deltaCurr2 C $capture1 U $capture")
    val auxil_size = capture1 * 4
println(s"AUXIL: $auxil_size AUXIL_IVERSE: $auxil_size")

 /**   temp9 = 0
    val indexTemp = new Array[VertexId](curr)
    val indexPairTemp = new Array[Int](curr)
    while ( temp9 < curr) {
       indexTemp(temp9) = index(temp9)
       indexPairTemp(temp9) = indexPair(temp9)
       temp9 += 1
    }
    // val cof = new CompressorStreamFactory()
    index = indexTemp
    indexPair = indexPairTemp
    val numParts = 1
    val byb = new ByteArrayOutputStream()
    val do1 = new DataOutputStream(byb)
    val bef11 = data(0)
    println(s"BEFORE: $bef11")
    do1.writeDouble(data(0).asInstanceOf[Double])
    do1.close()
    byb.close()
    val bybArr = byb.toByteArray()
    val bybIn = new ByteArrayInputStream(bybArr)
    val doIn = new DataInputStream(bybIn)
    val temp11 = doIn.readDouble()
    println(s"AFTER: $temp11")
    val vertSize: Array[Int] = new Array[Int](1)
    vertSize(0) = temp9
    var buf = new ByteArrayOutputStream()
 //   var compressor = new BZip2CompressorOutputStream(buf)
var compressor = new SnappyOutputStream(buf)
    var buf1 = new ByteArrayOutputStream()
    val compressor1 = new BZip2CompressorOutputStream(buf1)
    val size1 = localSrcIds.size
    val total = size1 *4
    val srcs = ByteBuffer.allocate(total)
    var i = 0
    val indexNext = new Array[Int](1)
    var y = 0
    while ( i < size1 ){
        srcs.putInt(localSrcIds(i))
        if ( i >= (y * (size1/numParts))){
           indexNext(y) = i
           y += 1
        }
        i += 1
    }
    i = 0
    val dsts = ByteBuffer.allocate(total)
    while ( i < size1 ){
        dsts.putInt(localDstIds(i))
        i += 1
    }
    val localSrcIdsCompressed = new Array[Array[Byte]](numParts)
    val localDstIdsCompressed = new Array[Array[Byte]](numParts)

    val srcArr = srcs.array
    val dstArr = dsts.array
    var z = 0
    buf = new ByteArrayOutputStream()
   // compressor = new BZip2CompressorOutputStream(buf)
compressor = new SnappyOutputStream(buf)

    while ( z < numParts) {
       buf = new ByteArrayOutputStream()
 //      compressor = new BZip2CompressorOutputStream(buf)
compressor = new SnappyOutputStream(buf)
//       val off = z * (size1/numParts) * 4
       var off = indexNext(z) * 4
       var len = total - off
       if ( z < numParts -1 ){
          len = (indexNext(z + 1) - indexNext(z)) * 4
       }
       compressor.write(srcArr, off, len)
       compressor.flush()
       buf.flush()
       compressor.close()
       buf.close()
       localSrcIdsCompressed(z) = buf.toByteArray()
       z += 1
    }
    z = 0
    while ( z < numParts) {
       var buf = new ByteArrayOutputStream()
     //  val compressor = new BZip2CompressorOutputStream(buf)
compressor = new SnappyOutputStream(buf)

       var off = indexNext(z) * 4
       var len = total - off
       if ( z < numParts -1 ) {
          len = (indexNext(z + 1) - indexNext(z)) * 4
       }

       compressor.write(dstArr, off, len)
       compressor.flush()
       buf.flush()
       compressor.close()
       buf.close()
       localDstIdsCompressed(z) = buf.toByteArray()
       z += 1
    }

    indexNext(0) = size1
    val l2gs = ByteBuffer.allocate(total * 2)
    val sizeL2gs = local2global.size
    var l2gIter = 0
    while ( l2gIter < sizeL2gs ) {
        l2gs.putLong(local2global(l2gIter).asInstanceOf[Long])
        l2gIter += 1
    }
    val l2gArr = l2gs.array()
    var bufl2 = new ByteArrayOutputStream()
    val compressorl2 = new BZip2CompressorOutputStream(bufl2)
    compressorl2.write(l2gArr)
    compressorl2.flush()
    bufl2.flush()
    compressorl2.close()
    bufl2.close()
    val l2gArrComp = bufl2.toByteArray()


    var typeData = -1
    data(0) match {
    case intVal: Int if classTag[ED] == classTag[Int] => {
          print("INTVAL")
          typeData = 1
    }
    case douVal: Double if classTag[ED] == classTag[Double] => {
          print("DOUV")
          typeData = 2
    }
    case _ => {
          print("NONE")
       }
    }

    val edgesDataArr = ByteBuffer.allocate(total)
    val dataSize = data.size
    var dataIter = 0
    while ( dataIter < dataSize ) {
 //       print("IN ITER")
        if ( typeData == 1) {
        edgesDataArr.putInt(data(dataIter).asInstanceOf[Int])
        }
        if ( typeData == 2) {
        edgesDataArr.putDouble(data(dataIter).asInstanceOf[Double])
        }
        dataIter += 1
    }
    val dataReal = new Array[ED](data.size)
    System.arraycopy(data, 0, dataReal, 0, data.size)
    val dataArrComp = edgesDataArr.array()
    var bufData = new ByteArrayOutputStream()
    val compressorData = new BZip2CompressorOutputStream(bufData)
    compressorData.write(dataArrComp)
    compressorData.flush()
    bufData.flush()
    compressorData.close()
    bufData.close()
    val dataComp = bufData.toByteArray()

    // edge data compression
    // val ba = new ByteArrayOutputStream()
    // val os = new ObjectOutputStream(ba)
    // os.writeObject(data)
    // os.flush()
    // os.close()
    // ba.flush()
    // ba.close()
    // val dataArr = ba.toByteArray()
    // var buf3 = new ByteArrayOutputStream()
    // val compressor2 = new BZip2CompressorOutputStream(buf3)
    // compressor2.write(dataArr)
    // compressor2.flush()
    // buf3.flush()
    // compressor2.close()
    // buf3.close()
    // val dataArrCompressed = buf3.toByteArray()
    // vertex data compression
    // val ba1 = new ByteArrayOutputStream()
    // var bb45 = ByteBuffer.wrap(dataArrCompressed)
    // match vertexAttrs(0){
    //   case Int => z = 0
    //  case Double => z = 1
    //  case Long => z = 2
    // }
    // if ( z == 0)
      // for( int i = 0; i < vertexAttrs.size; i ++)
       //    bb45.writeInt(vertexAttrs(i).asInstanceOf[Integer]);
    // else if ( z ==1 )
    // var z = 0
    // while ( z < vertexAttrs.size ){
    //    bb45.putInt(vertexAttrs(z).asInstanceOf[Integer])
    //    z += 1
    // }
    // println(s"ZIS: $z")
    val check1 = vertexAttrs(0)
    println(s"VERT0 $check1")
    val c2 = vertexAttrs(0).getClass().getSimpleName()
    println(s"VER0CLASS $c2")
    // val os1 = new ObjectOutputStream(ba1)
    // os1.writeObject(vertexAttrs)
    // os1.flush()
    // os1.close()
    // val vertexDataArr = bb45.array()
    // var buf4 = new ByteArrayOutputStream()
    // val compressor3 = new BZip2CompressorOutputStream(buf4)
    // compressor3.write(vertexDataArr)
    // compressor3.flush()
    // buf4.flush()
    // compressor3.close()
    // buf4.close()
    // val vertexDataArrCompressed = buf4.toByteArray()
    // local2global data compression
     val ba2 = new ByteArrayOutputStream()
     val os2 = new ObjectOutputStream(ba2)
    // os2.writeObject(local2global.trim().array)
    // os2.flush()
    // os2.close()
    // ba2.flush()
    // ba2.close()
    // val local2globalArr = ba2.toByteArray()
    // var buf5 = new ByteArrayOutputStream()
    // val compressor4 = new BZip2CompressorOutputStream(buf5)
    // compressor4.write(local2globalArr)
    // compressor4.flush()
    // buf5.flush()
    // compressor4.close()
    // buf5.close()
    // val local2globalArrCompressed = buf5.toByteArray()
    // val src_size = localSrcIds.size * 4
    // val dst_size = localDstIds.size * 4
    // val src_size_comp = localSrcIdsCompressed.size
    // val dst_size_comp = localDstIdsCompressed.size
    // println(s"SRC SIZE: $src_size COMPRESSED: $src_size_comp")
    // println(s"DST SIZE: $dst_size COMPRESSED: $dst_size_comp")
    // val data_size = data.size * 4
    // val data_size_comp = dataArrCompressed.size
    // println(s"E DATA SIZE: $data_size COMPRESSED: $data_size_comp")
    // val l2g_size = local2global.trim().array.size * 4
    // val l2g_size_comp = local2globalArrCompressed.size
    // println(s"L2G SIZE: $l2g_size COMPRESSED: $l2g_size_comp")
    // val vert_size = vertexAttrs.size * 4
    // val vert_size_comp = vertexDataArrCompressed.size
    // println(s"V DATA SIZE: $vert_size COMPRESSED: $vert_size_comp")
 */
    val ar = new BitSet(10)
    new EdgePartition(
      localSrcIds, localDstIds,
      data, index1, global2local1,
      local2global.trim().array, vertexAttrs, None, None,
      RdeltaOrig1,
      RdeltaSrc1,
      deltaSrc,
RdeltaOrig2, deltaIndex2,
      auxil, auxil_inverse)
  }
}

/**
 * Constructs an EdgePartition from an existing EdgePartition with the same vertex set. This enables
 * reuse of the local vertex ids. Intended for internal use in EdgePartition only.
 */
private[impl]
class ExistingEdgePartitionBuilder[
    @specialized(Long, Int, Double) ED: ClassTag, VD: ClassTag](
    global2local: GraphXPrimitiveKeyOpenHashMap[VertexId, Int],
    local2global: Array[VertexId],
    vertexAttrs: Array[VD],
    activeSet: Option[VertexSet],
    size: Int = 64) {
  private[this] val edges = new PrimitiveVector[EdgeWithLocalIds[ED]](size)
  /** Add a new edge to the partition. */
  def add(src: VertexId, dst: VertexId, localSrc: Int, localDst: Int, d: ED) {
    edges += EdgeWithLocalIds(src, dst, localSrc, localDst, d)
  }

  def toEdgePartition: EdgePartition[ED, VD] = {
    println("in esisting")
    val edgeArray = edges.trim().array
    new Sorter(EdgeWithLocalIds.edgeArraySortDataFormat[ED])
      .sort(edgeArray, 0, edgeArray.length, EdgeWithLocalIds.lexicographicOrdering)
    val localSrcIds = new Array[Int](edgeArray.size)
    val localDstIds = new Array[Int](edgeArray.size)
    val data = new Array[ED](edgeArray.size)
 //   val index = new GraphXPrimitiveKeyOpenHashMap[VertexId, Int]
    var index = new Array[VertexId](edgeArray.size)
    var indexPair = new Array[Int](edgeArray.size)

    var curr = 1
    index(0) = edgeArray(0).srcId
    indexPair(0) = 0
    // Copy edges into columnar structures, tracking the beginnings of source vertex id clusters and
    // adding them to the index
    if (edgeArray.length > 0) {
     // index.update(edgeArray(0).srcId, 0)
      var currSrcId: VertexId = edgeArray(0).srcId
      var i = 0
      while (i < edgeArray.size) {
        localSrcIds(i) = edgeArray(i).localSrcId
        localDstIds(i) = edgeArray(i).localDstId
        data(i) = edgeArray(i).attr
        if (edgeArray(i).srcId != currSrcId) {
          currSrcId = edgeArray(i).srcId
 //         index.update(currSrcId, i)
          index(curr) = currSrcId
          indexPair(curr) = i
        }
        i += 1
      }
    }
    var temp9 = 0
    val indexTemp = new Array[VertexId](curr)
    val indexPairTemp = new Array[Int](curr)
    while ( temp9 < curr) {
       indexTemp(temp9) = index(temp9)
       indexPairTemp(temp9) = indexPair(temp9)
       temp9 += 1
    }
    index = indexTemp
    indexPair = indexPairTemp

    println(s"IN2ND")
    val vertSize = new Array[Int](1)
    vertSize(0) = vertexAttrs.size
    var buf1 = new ByteArrayOutputStream()
    val compressor1 = new BZip2CompressorOutputStream(buf1)
    val size1 = localSrcIds.size
    val total = size1 *4
    val srcs = ByteBuffer.allocate(total)
    var i = 0
    var y = 0
    var indexNext = new Array[Int](1)
    while ( i < size1 ){
        srcs.putInt(localSrcIds(i))
        if ( i >= (y * size1/4)){
           indexNext(y) = i
           y += 1
        }

        i += 1
    }
    i = 0
    val dsts = ByteBuffer.allocate(total)
    while ( i < size1 ){
        dsts.putInt(localDstIds(i))
        i += 1
    }
    val numParts = 1
    val localSrcIdsCompressed = new Array[Array[Byte]](numParts)
    val localDstIdsCompressed = new Array[Array[Byte]](numParts)

    val srcArr = srcs.array
    val dstArr = dsts.array
    var z = 0
    var buf = new ByteArrayOutputStream()
    var compressor = new SnappyOutputStream(buf)
    while ( z < numParts) {
       buf = new ByteArrayOutputStream()
       compressor = new SnappyOutputStream(buf)
       var off = indexNext(z) * 4
       var len = total - off
       if ( z < numParts -1 ){
          len = (indexNext(z + 1) - indexNext(z)) * 4
       }

       compressor.write(srcArr, off, len)
       compressor.flush()
       buf.flush()
       compressor.close()
       buf.close()
       localSrcIdsCompressed(z) = buf.toByteArray()
       z += 1
    }
    z = 0
    while ( z < numParts) {
       var buf = new ByteArrayOutputStream()
       val compressor = new BZip2CompressorOutputStream(buf)
       var off = indexNext(z) * 4
       var len = total - off
       if ( z < numParts -1 ){
          len = (indexNext(z + 1) - indexNext(z)) * 4
       }
       compressor.write(dstArr, off, len)
       compressor.flush()
       buf.flush()
       compressor.close()
       buf.close()
       localDstIdsCompressed(z) = buf.toByteArray()
       z += 1
    }

    indexNext(0) = size1
    val l2gs = ByteBuffer.allocate(total * 2)
    val sizeL2gs = local2global.size
    var l2gIter = 0
    while ( l2gIter < sizeL2gs ) {
        l2gs.putLong(local2global(l2gIter).asInstanceOf[Long])
        l2gIter += 1
    }
    val l2gArr = l2gs.array()
    var bufl2 = new ByteArrayOutputStream()
    val compressorl2 = new BZip2CompressorOutputStream(bufl2)
    compressorl2.write(l2gArr)
    compressorl2.flush()
    bufl2.flush()
    compressorl2.close()
    bufl2.close()
    val l2gArrComp = bufl2.toByteArray()

    var typeData = -1
    data(0) match {
    case intVal: Int if classTag[ED] == classTag[Int] => {
          print("INTVAL")
          typeData = 1
    }
    case douVal: Double if classTag[ED] == classTag[Double] => {
          print("DOUV")
          typeData = 2
    }
    case _ => {
          print("NONE")
       }
    }

    val edgesDataArr = ByteBuffer.allocate(total * 2)
    val dataSize = data.size
    var dataIter = 0
    while ( dataIter < dataSize ) {
        if ( typeData == 1) {
        edgesDataArr.putInt(data(dataIter).asInstanceOf[Int])
        }
        if ( typeData == 2) {
        edgesDataArr.putDouble(data(dataIter).asInstanceOf[Double])
        }
        l2gIter += 1
    }

    val dataArrComp = edgesDataArr.array()
    var bufData = new ByteArrayOutputStream()
    val compressorData = new BZip2CompressorOutputStream(bufData)
    compressorData.write(dataArrComp)
    compressorData.flush()
    bufData.flush()
    compressorData.close()
    bufData.close()
    val dataComp = bufData.toByteArray()

    // edge data compression
    val ba = new ByteArrayOutputStream()
    val os = new ObjectOutputStream(ba)
    os.writeObject(data)
    os.flush()
    os.close()
    ba.flush()
    ba.close()
    val dataArr = ba.toByteArray()
    var buf3 = new ByteArrayOutputStream()
    val compressor2 = new BZip2CompressorOutputStream(buf3)
    compressor2.write(dataArr)
    compressor2.flush()
    buf3.flush()
    compressor2.close()
    buf3.close()
    val dataArrCompressed = buf3.toByteArray()
    // vertex data compression
    val ba1 = new ByteArrayOutputStream()
    val os1 = new ObjectOutputStream(ba1)
    os1.writeObject(vertexAttrs)
    os1.flush()
    os1.close()
    ba1.flush()
    ba1.close()
    val vertexDataArr = ba1.toByteArray()
    var buf4 = new ByteArrayOutputStream()
    val compressor3 = new BZip2CompressorOutputStream(buf4)
    compressor3.write(vertexDataArr)
    compressor3.flush()
    buf4.flush()
    compressor3.close()
    buf4.close()
    val vertexDataArrCompressed = buf4.toByteArray()
    // local2global data compression
    val ba2 = new ByteArrayOutputStream()
    val os2 = new ObjectOutputStream(ba2)
    os2.writeObject(local2global)
    os2.flush()
    os2.close()
    ba2.flush()
    ba2.close()
    val local2globalArr = ba2.toByteArray()
    var buf5 = new ByteArrayOutputStream()
    val compressor4 = new BZip2CompressorOutputStream(buf5)
    compressor4.write(local2globalArr)
    compressor4.flush()
    buf5.flush()
    compressor4.close()
    buf5.close()
    val local2globalArrCompressed = buf5.toByteArray()
    val src_size = localSrcIds.size * 4
    val dst_size = localDstIds.size * 4
    val src_size_comp = localSrcIdsCompressed.size
    val dst_size_comp = localDstIdsCompressed.size
    println(s"SRC SIZE: $src_size COMPRESSED: $src_size_comp")
    println(s"DST SIZE: $dst_size COMPRESSED: $dst_size_comp")
    val data_size = data.size * 4
    val data_size_comp = dataArrCompressed.size
    println(s"E DATA SIZE: $data_size COMPRESSED: $data_size_comp")
    val l2g_size = local2global.size * 4
    val l2g_size_comp = local2globalArrCompressed.size
    println(s"L2G SIZE: $l2g_size COMPRESSED: $l2g_size_comp")
    val vert_size = vertexAttrs.size * 4
    val vert_size_comp = vertexDataArrCompressed.size
    println(s"V DATA SIZE: $vert_size COMPRESSED: $vert_size_comp")
    val dataReal = new Array[ED](data.size)
    System.arraycopy(data, 0, dataReal, 0, data.size)
    var areme = new Array[Byte](10)
    var areme1 = new BitSet(10)

    new EdgePartition(
      localSrcIds, localDstIds,
      data, null, global2local,
      local2global, vertexAttrs, None, None, localSrcIds,
      areme,
      areme1,
      local2global,
      areme, localSrcIds, localSrcIds)
  }
}

private[impl] case class EdgeWithLocalIds[@specialized ED](
    srcId: VertexId, dstId: VertexId, localSrcId: Int, localDstId: Int, attr: ED)

private[impl] object EdgeWithLocalIds {
  implicit def lexicographicOrdering[ED]: Ordering[EdgeWithLocalIds[ED]] =
    new Ordering[EdgeWithLocalIds[ED]] {
      override def compare(a: EdgeWithLocalIds[ED], b: EdgeWithLocalIds[ED]): Int = {
        if (a.srcId == b.srcId) {
          if (a.dstId == b.dstId) 0
          else if (a.dstId < b.dstId) -1
          else 1
        } else if (a.srcId < b.srcId) -1
        else 1
      }
    }

  private[graphx] def edgeArraySortDataFormat[ED] = {
    new SortDataFormat[EdgeWithLocalIds[ED], Array[EdgeWithLocalIds[ED]]] {
      override def getKey(data: Array[EdgeWithLocalIds[ED]], pos: Int): EdgeWithLocalIds[ED] = {
        data(pos)
      }

      override def swap(data: Array[EdgeWithLocalIds[ED]], pos0: Int, pos1: Int): Unit = {
        val tmp = data(pos0)
        data(pos0) = data(pos1)
        data(pos1) = tmp
      }

      override def copyElement(
          src: Array[EdgeWithLocalIds[ED]], srcPos: Int,
          dst: Array[EdgeWithLocalIds[ED]], dstPos: Int) {
        dst(dstPos) = src(srcPos)
      }

      override def copyRange(
          src: Array[EdgeWithLocalIds[ED]], srcPos: Int,
          dst: Array[EdgeWithLocalIds[ED]], dstPos: Int, length: Int) {
        System.arraycopy(src, srcPos, dst, dstPos, length)
      }

      override def allocate(length: Int): Array[EdgeWithLocalIds[ED]] = {
        new Array[EdgeWithLocalIds[ED]](length)
      }
    }
  }
}
// scalastyle:on println
