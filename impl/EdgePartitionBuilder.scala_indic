/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.spark.graphx.impl

import scala.reflect.ClassTag

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream
import java.io.ByteArrayOutputStream
import java.io.ByteArrayInputStream
import java.io.DataOutputStream
import java.io.DataInputStream
import java.nio.ByteBuffer
import java.io.ObjectOutputStream
import org.apache.spark.graphx._
import org.apache.spark.graphx.util.collection.GraphXPrimitiveKeyOpenHashMap
import org.apache.spark.util.collection.{SortDataFormat, Sorter, PrimitiveVector}
// scalastyle:off println
/** Constructs an EdgePartition from scratch. */
private[graphx]
class EdgePartitionBuilder[@specialized(Long, Int, Double) ED: ClassTag, VD: ClassTag](
    size: Int = 64) {
  private[this] val edges = new PrimitiveVector[Edge[ED]](size)

  /** Add a new edge to the partition. */
  def add(src: VertexId, dst: VertexId, d: ED) {
    edges += Edge(src, dst, d)
  }

  def toEdgePartition: EdgePartition[ED, VD] = {
    val edgeArray = edges.trim().array
    new Sorter(Edge.edgeArraySortDataFormat[ED])
      .sort(edgeArray, 0, edgeArray.length, Edge.lexicographicOrdering)
    val localSrcIds = new Array[Int](edgeArray.size)
    val localDstIds = new Array[Int](edgeArray.size)
    val data = new Array[ED](edgeArray.size)
    val index = new GraphXPrimitiveKeyOpenHashMap[VertexId, Int]
    val global2local = new GraphXPrimitiveKeyOpenHashMap[VertexId, Int]
    val local2global = new PrimitiveVector[VertexId]
    var vertexAttrs = Array.empty[VD]
    var temp9 = 0
    // Copy edges into columnar structures, tracking the beginnings of source vertex id clusters and
    // adding them to the index. Also populate a map from vertex id to a sequential local offset.
    if (edgeArray.length > 0) {
      index.update(edgeArray(0).srcId, 0)
      var currSrcId: VertexId = edgeArray(0).srcId
      var currLocalId = -1
      var i = 0
      while (i < edgeArray.size) {
        val srcId = edgeArray(i).srcId
        val dstId = edgeArray(i).dstId
        localSrcIds(i) = global2local.changeValue(srcId,
          { currLocalId += 1; local2global += srcId; currLocalId }, identity)
        localDstIds(i) = global2local.changeValue(dstId,
          { currLocalId += 1; local2global += dstId; currLocalId }, identity)
        data(i) = edgeArray(i).attr
        if (srcId != currSrcId) {
          currSrcId = srcId
          index.update(currSrcId, i)
        }

        i += 1
      }
      vertexAttrs = new Array[VD](currLocalId + 1)
      temp9 = currLocalId + 1
    }
    val numParts = 4
    val byb = new ByteArrayOutputStream()
    val do1 = new DataOutputStream(byb)
    val bef11 = data(0)
    println(s"BEFORE: $bef11")
    do1.writeDouble(data(0).asInstanceOf[Double])
    do1.close()
    byb.close()
    val bybArr = byb.toByteArray()
    val bybIn = new ByteArrayInputStream(bybArr)
    val doIn = new DataInputStream(bybIn)
    val temp11 = doIn.readDouble()
    println(s"AFTER: $temp11")
    val vertSize: Array[Int] = new Array[Int](1)
    vertSize(0) = temp9
    var buf = new ByteArrayOutputStream()
    val compressor = new BZip2CompressorOutputStream(buf)
    var buf1 = new ByteArrayOutputStream()
    val compressor1 = new BZip2CompressorOutputStream(buf1)
    val size1 = localSrcIds.size
    val total = size1 *4
    val srcs = ByteBuffer.allocate(total)
    var i = 0
    while ( i < size1 ){
        srcs.putInt(localSrcIds(i))
        i += 1
    }
    i = 0
    val dsts = ByteBuffer.allocate(total)
    while ( i < size1 ){
        dsts.putInt(localDstIds(i))
        i += 1
    }
    val localSrcIdsCompressed = new Array.ofDim[Byte](numParts, 1)
    val localDstIdsCompressed = new Array.ofDim[Byte](numParts, 1)

    val srcArr = srcs.array
    val dstArr = dsts.array
    var z = 0
    var buf = new ByteArrayOutputStream()
    val compressor = new BZip2CompressorOutputStream(buf)
    while ( z < numParts) {
       var buf = new ByteArrayOutputStream()
       val compressor = new BZip2CompressorOutputStream(buf)
       val off = z * (size1/numParts) * 4
       val len = off + ((size1/numParts) * 4)
       compressor.write(srcArr, off, len)
       compressor.flush()
       buf.flush()
       compressor.close()
       buf.close()
       localSrcIdsCompressed(z) = buf.toByteArray()
    }
    z = 0
    while ( z < numParts) {
       var buf = new ByteArrayOutputStream()
       val compressor = new BZip2CompressorOutputStream(buf)
       val off = z * (size1/numParts) * 4
       val len = off + ((size1/numParts) * 4)
       compressor.write(dstArr, off, len)
       compressor.flush()
       buf.flush()
       compressor.close()
       buf.close()
       localDstIdsCompressed(z) = buf.toByteArray()
    }

    val sizeArr: Array[Int] = new Array[Int](1)
    sizeArr(0) = size1
    // edge data compression
    // val ba = new ByteArrayOutputStream()
    // val os = new ObjectOutputStream(ba)
    // os.writeObject(data)
    // os.flush()
    // os.close()
    // ba.flush()
    // ba.close()
    // val dataArr = ba.toByteArray()
    // var buf3 = new ByteArrayOutputStream()
    // val compressor2 = new BZip2CompressorOutputStream(buf3)
    // compressor2.write(dataArr)
    // compressor2.flush()
    // buf3.flush()
    // compressor2.close()
    // buf3.close()
    // val dataArrCompressed = buf3.toByteArray()
    // vertex data compression
    // val ba1 = new ByteArrayOutputStream()
    // var bb45 = ByteBuffer.wrap(dataArrCompressed)
    // match vertexAttrs(0){
    //   case Int => z = 0
    //  case Double => z = 1
    //  case Long => z = 2
    // }
    // if ( z == 0)
      // for( int i = 0; i < vertexAttrs.size; i ++)
       //    bb45.writeInt(vertexAttrs(i).asInstanceOf[Integer]);
    // else if ( z ==1 )
    // var z = 0
    // while ( z < vertexAttrs.size ){
    //    bb45.putInt(vertexAttrs(z).asInstanceOf[Integer])
    //    z += 1
    // }
    // println(s"ZIS: $z")
    val check1 = vertexAttrs(0)
    println(s"VERT0 $check1")
    val c2 = vertexAttrs(0).getClass().getSimpleName()
    println(s"VER0CLASS $c2")
    // val os1 = new ObjectOutputStream(ba1)
    // os1.writeObject(vertexAttrs)
    // os1.flush()
    // os1.close()
    // val vertexDataArr = bb45.array()
    // var buf4 = new ByteArrayOutputStream()
    // val compressor3 = new BZip2CompressorOutputStream(buf4)
    // compressor3.write(vertexDataArr)
    // compressor3.flush()
    // buf4.flush()
    // compressor3.close()
    // buf4.close()
    // val vertexDataArrCompressed = buf4.toByteArray()
    // local2global data compression
    // val ba2 = new ByteArrayOutputStream()
    // val os2 = new ObjectOutputStream(ba2)
    // os2.writeObject(local2global.trim().array)
    // os2.flush()
    // os2.close()
    // ba2.flush()
    // ba2.close()
    // val local2globalArr = ba2.toByteArray()
    // var buf5 = new ByteArrayOutputStream()
    // val compressor4 = new BZip2CompressorOutputStream(buf5)
    // compressor4.write(local2globalArr)
    // compressor4.flush()
    // buf5.flush()
    // compressor4.close()
    // buf5.close()
    // val local2globalArrCompressed = buf5.toByteArray()
    // val src_size = localSrcIds.size * 4
    // val dst_size = localDstIds.size * 4
    // val src_size_comp = localSrcIdsCompressed.size
    // val dst_size_comp = localDstIdsCompressed.size
    // println(s"SRC SIZE: $src_size COMPRESSED: $src_size_comp")
    // println(s"DST SIZE: $dst_size COMPRESSED: $dst_size_comp")
    // val data_size = data.size * 4
    // val data_size_comp = dataArrCompressed.size
    // println(s"E DATA SIZE: $data_size COMPRESSED: $data_size_comp")
    // val l2g_size = local2global.trim().array.size * 4
    // val l2g_size_comp = local2globalArrCompressed.size
    // println(s"L2G SIZE: $l2g_size COMPRESSED: $l2g_size_comp")
    // val vert_size = vertexAttrs.size * 4
    // val vert_size_comp = vertexDataArrCompressed.size
    // println(s"V DATA SIZE: $vert_size COMPRESSED: $vert_size_comp")

    new EdgePartition(
      localSrcIdsCompressed,
      localDstIdsCompressed, data,
      index, global2local,
      local2global.trim().array,
      vertexAttrs, None,
      sizeArr, vertSize)
  }
}

/**
 * Constructs an EdgePartition from an existing EdgePartition with the same vertex set. This enables
 * reuse of the local vertex ids. Intended for internal use in EdgePartition only.
 */
private[impl]
class ExistingEdgePartitionBuilder[
    @specialized(Long, Int, Double) ED: ClassTag, VD: ClassTag](
    global2local: GraphXPrimitiveKeyOpenHashMap[VertexId, Int],
    local2global: Array[VertexId],
    vertexAttrs: Array[VD],
    activeSet: Option[VertexSet],
    size: Int = 64) {
  private[this] val edges = new PrimitiveVector[EdgeWithLocalIds[ED]](size)

  /** Add a new edge to the partition. */
  def add(src: VertexId, dst: VertexId, localSrc: Int, localDst: Int, d: ED) {
    edges += EdgeWithLocalIds(src, dst, localSrc, localDst, d)
  }

  def toEdgePartition: EdgePartition[ED, VD] = {
    val edgeArray = edges.trim().array
    new Sorter(EdgeWithLocalIds.edgeArraySortDataFormat[ED])
      .sort(edgeArray, 0, edgeArray.length, EdgeWithLocalIds.lexicographicOrdering)
    val localSrcIds = new Array[Int](edgeArray.size)
    val localDstIds = new Array[Int](edgeArray.size)
    val data = new Array[ED](edgeArray.size)
    val index = new GraphXPrimitiveKeyOpenHashMap[VertexId, Int]
    // Copy edges into columnar structures, tracking the beginnings of source vertex id clusters and
    // adding them to the index
    if (edgeArray.length > 0) {
      index.update(edgeArray(0).srcId, 0)
      var currSrcId: VertexId = edgeArray(0).srcId
      var i = 0
      while (i < edgeArray.size) {
        localSrcIds(i) = edgeArray(i).localSrcId
        localDstIds(i) = edgeArray(i).localDstId
        data(i) = edgeArray(i).attr
        if (edgeArray(i).srcId != currSrcId) {
          currSrcId = edgeArray(i).srcId
          index.update(currSrcId, i)
        }
        i += 1
      }
    }
    println(s"IN2ND")
    val vertSize = new Array[Int](1)
    vertSize(0) = vertexAttrs.size
    var buf = new ByteArrayOutputStream()
    val compressor = new BZip2CompressorOutputStream(buf)
    var buf1 = new ByteArrayOutputStream()
    val compressor1 = new BZip2CompressorOutputStream(buf1)
    val size1 = localSrcIds.size
    val total = size1 *4
    val srcs = ByteBuffer.allocate(total)
    var i = 0
    while ( i < size1 ){
        srcs.putInt(localSrcIds(i))
        i += 1
    }
    i = 0
    val dsts = ByteBuffer.allocate(total)
    while ( i < size1 ){
        dsts.putInt(localDstIds(i))
        i += 1
    }
    val srcArr = srcs.array
    val dstArr = dsts.array
    compressor.write(srcArr, 0, total)
    compressor.flush()
    buf.flush()
    compressor.close()
    buf.close()
    compressor1.write(dstArr, 0, total)
    compressor1.flush()
    buf1.flush()
    compressor1.close()
    buf1.close()
    val localSrcIdsCompressed = buf.toByteArray()
    val localDstIdsCompressed = buf1.toByteArray()
    val sizeArr: Array[Int] = new Array[Int](1)
    sizeArr(0) = size1
    // edge data compression
    val ba = new ByteArrayOutputStream()
    val os = new ObjectOutputStream(ba)
    os.writeObject(data)
    os.flush()
    os.close()
    ba.flush()
    ba.close()
    val dataArr = ba.toByteArray()
    var buf3 = new ByteArrayOutputStream()
    val compressor2 = new BZip2CompressorOutputStream(buf3)
    compressor2.write(dataArr)
    compressor2.flush()
    buf3.flush()
    compressor2.close()
    buf3.close()
    val dataArrCompressed = buf3.toByteArray()
    // vertex data compression
    val ba1 = new ByteArrayOutputStream()
    val os1 = new ObjectOutputStream(ba1)
    os1.writeObject(vertexAttrs)
    os1.flush()
    os1.close()
    ba1.flush()
    ba1.close()
    val vertexDataArr = ba1.toByteArray()
    var buf4 = new ByteArrayOutputStream()
    val compressor3 = new BZip2CompressorOutputStream(buf4)
    compressor3.write(vertexDataArr)
    compressor3.flush()
    buf4.flush()
    compressor3.close()
    buf4.close()
    val vertexDataArrCompressed = buf4.toByteArray()
    // local2global data compression
    val ba2 = new ByteArrayOutputStream()
    val os2 = new ObjectOutputStream(ba2)
    os2.writeObject(local2global)
    os2.flush()
    os2.close()
    ba2.flush()
    ba2.close()
    val local2globalArr = ba2.toByteArray()
    var buf5 = new ByteArrayOutputStream()
    val compressor4 = new BZip2CompressorOutputStream(buf5)
    compressor4.write(local2globalArr)
    compressor4.flush()
    buf5.flush()
    compressor4.close()
    buf5.close()
    val local2globalArrCompressed = buf5.toByteArray()
    val src_size = localSrcIds.size * 4
    val dst_size = localDstIds.size * 4
    val src_size_comp = localSrcIdsCompressed.size
    val dst_size_comp = localDstIdsCompressed.size
    println(s"SRC SIZE: $src_size COMPRESSED: $src_size_comp")
    println(s"DST SIZE: $dst_size COMPRESSED: $dst_size_comp")
    val data_size = data.size * 4
    val data_size_comp = dataArrCompressed.size
    println(s"E DATA SIZE: $data_size COMPRESSED: $data_size_comp")
    val l2g_size = local2global.size * 4
    val l2g_size_comp = local2globalArrCompressed.size
    println(s"L2G SIZE: $l2g_size COMPRESSED: $l2g_size_comp")
    val vert_size = vertexAttrs.size * 4
    val vert_size_comp = vertexDataArrCompressed.size
    println(s"V DATA SIZE: $vert_size COMPRESSED: $vert_size_comp")

    new EdgePartition(
      localSrcIdsCompressed,
      localDstIdsCompressed,
      data, index,
      global2local, local2global,
      vertexAttrs, activeSet,
      sizeArr, vertSize)
  }
}

private[impl] case class EdgeWithLocalIds[@specialized ED](
    srcId: VertexId, dstId: VertexId, localSrcId: Int, localDstId: Int, attr: ED)

private[impl] object EdgeWithLocalIds {
  implicit def lexicographicOrdering[ED]: Ordering[EdgeWithLocalIds[ED]] =
    new Ordering[EdgeWithLocalIds[ED]] {
      override def compare(a: EdgeWithLocalIds[ED], b: EdgeWithLocalIds[ED]): Int = {
        if (a.srcId == b.srcId) {
          if (a.dstId == b.dstId) 0
          else if (a.dstId < b.dstId) -1
          else 1
        } else if (a.srcId < b.srcId) -1
        else 1
      }
    }

  private[graphx] def edgeArraySortDataFormat[ED] = {
    new SortDataFormat[EdgeWithLocalIds[ED], Array[EdgeWithLocalIds[ED]]] {
      override def getKey(data: Array[EdgeWithLocalIds[ED]], pos: Int): EdgeWithLocalIds[ED] = {
        data(pos)
      }

      override def swap(data: Array[EdgeWithLocalIds[ED]], pos0: Int, pos1: Int): Unit = {
        val tmp = data(pos0)
        data(pos0) = data(pos1)
        data(pos1) = tmp
      }

      override def copyElement(
          src: Array[EdgeWithLocalIds[ED]], srcPos: Int,
          dst: Array[EdgeWithLocalIds[ED]], dstPos: Int) {
        dst(dstPos) = src(srcPos)
      }

      override def copyRange(
          src: Array[EdgeWithLocalIds[ED]], srcPos: Int,
          dst: Array[EdgeWithLocalIds[ED]], dstPos: Int, length: Int) {
        System.arraycopy(src, srcPos, dst, dstPos, length)
      }

      override def allocate(length: Int): Array[EdgeWithLocalIds[ED]] = {
        new Array[EdgeWithLocalIds[ED]](length)
      }
    }
  }
}
// scalastyle:on println
