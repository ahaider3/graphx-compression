/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.spark.graphx.impl
import net.jpountz.lz4._
import org.xerial.snappy.{Snappy, SnappyInputStream, SnappyOutputStream}
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream
import java.io.ByteArrayOutputStream
import java.io.ByteArrayInputStream
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.EOFException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import scala.reflect.{classTag, ClassTag}
import java.nio.ByteBuffer
import org.apache.spark.graphx._
import org.apache.spark.graphx.util.collection.GraphXPrimitiveKeyOpenHashMap
import org.apache.spark.util.collection.BitSet
// scalastyle:off println
/**
 * A collection of edges, along with referenced vertex attributes and an optional active vertex set
 * for filtering computation on the edges.
 *
 * The edges are stored in columnar format in `localSrcIds`, `localDstIds`, and `data`. All
 * referenced global vertex ids are mapped to a compact set of local vertex ids according to the
 * `global2local` map. Each local vertex id is a valid index into `vertexAttrs`, which stores the
 * corresponding vertex attribute, and `local2global`, which stores the reverse mapping to global
 * vertex id. The global vertex ids that are active are optionally stored in `activeSet`.
 *
 * The edges are clustered by source vertex id, and the mapping from global vertex id to the index
 * of the corresponding edge cluster is stored in `index`.
 *
 * @tparam ED the edge attribute type
 * @tparam VD the vertex attribute type
 *
 * @param localSrcIds the local source vertex id of each edge as an index into `local2global` and
 *   `vertexAttrs`
 * @param localDstIds the local destination vertex id of each edge as an index into `local2global`
 *   and `vertexAttrs`
 * @param data the attribute associated with each edge
 * @param index a clustered index on source vertex id as a map from each global source vertex id to
 *   the offset in the edge arrays where the cluster for that vertex id begins
 * @param global2local a map from referenced vertex ids to local ids which index into vertexAttrs
 * @param local2global an array of global vertex ids where the offsets are local vertex ids
 * @param vertexAttrs an array of vertex attributes where the offsets are local vertex ids
 * @param activeSet an optional active vertex set for filtering computation on the edges
 */
private[graphx]
class EdgePartition[
    @specialized(Char, Int, Boolean, Byte, Long, Float, Double) ED: ClassTag, VD: ClassTag](
    localDstIds1: Array[Int],
    localSrcIds: BitSet,
    localDstIds: Array[Byte],
    data: Array[ED],
    index: Array[Byte],
    indexPair: Array[VertexId],
    auxil: Array[Int],
    vertexAttrs: Array[VD],
    activeset: Option[BitSet])
  extends Serializable {
  var accumDe: Long = 0
  /** No-arg constructor for serialization. */
  private def this() = this(null, null, null, null, null, null, null, null, null)

  /** Return a new `EdgePartition` with the specified edge data. */
  def withData[ED2: ClassTag](data: Array[ED2]): EdgePartition[ED2, VD] = {
    println("WITHDATA")
    val dataComp = new Array[ED2](data.size)
    System.arraycopy(data, 0, dataComp, 0, data.size)
   // val ba = new ByteArrayOutputStream()
   // val os = new ObjectOutputStream(ba)
   // os.writeObject(data)
   // os.flush()
  //  os.close()
  //  ba.flush()
  //  ba.close()
  //  val dataArr = ba.toByteArray()
  //  var buf3 = new ByteArrayOutputStream()
  //  val compressor2 = new BZip2CompressorOutputStream(buf3)
  // /a/  compressor2.write(dataArr)
  //  compressor2.flush()
  //  buf3.flush()
   // compressor2.close()
  //  buf3.close()
  //  val dataArrCompressed = buf3.toByteArray()
   // println(s"IN WITH DATA")
    new EdgePartition( localDstIds1,
 localSrcIds, localDstIds, data, index, indexPair,
      auxil,
      vertexAttrs, activeset)
  }

  /** Return a new `EdgePartition` with the specified active set, provided as an iterator. */
  def withActiveSet(iter: Iterator[VertexId]): EdgePartition[ED, VD] = {
    println(s"WITHACTIVE")
 //   val activeSet = new VertexSet
    val activeset = new BitSet(auxil.size)
    var j = 0
    var arr = new Array[Long](auxil.size)
    while (iter.hasNext) {
   //    activeSet.add(iter.next())
         val temp = iter.next()
         arr(j) = temp.asInstanceOf[Long]
       j += 1 }
    var arr1 = new Array[Long](j)
    System.arraycopy(arr, 0, arr1, 0, j)
  //  sort(arr1)
    var currSum: Long = indexPair(0)
    var curr = 0
    j = 0
    var flag = true
    while( j < arr1.size) {
      if (flag){
        currSum = currSum + index(j)
      }
    if ( currSum == arr1(curr)){
         activeset.set(j)
         curr += 1
         j += 1
         flag = true
    }
    else if ( currSum < arr1(curr)){
         j += 1
         flag = true
    }
    else {
         curr += 1
         flag = false
    }
    }
    println(s"VERTSETSIZE: $j")
    new EdgePartition( localDstIds1,
localSrcIds, localDstIds, data, index, indexPair,
      auxil, vertexAttrs,
      Some(activeset))
  }

  /** Return a new `EdgePartition` with updates to vertex attributes specified in `iter`. */
  def updateVertices(iter: Iterator[(VertexId, VD)]): EdgePartition[ED, VD] = {
    // if iterator.size/vertdata.size > threshold
    // decompress and append all data
    // else
    // add values to ht.
     val rt = Runtime.getRuntime()
     val tempSet = new VertexSet
     var tot = rt.totalMemory()
     var free = rt.freeMemory()
     var used = tot - free
     println(s"TOTALVERT: $tot FREEVERT: $free USEDVERT: $used")
    var newVertexAttrs = new Array[VD](vertexAttrs.size)
    System.arraycopy(vertexAttrs, 0, newVertexAttrs, 0, vertexAttrs.size)
    var j = 0
    while (iter.hasNext) {
      val kv = iter.next()
     // newVertexAttrs(global2local(kv._1)) = kv._2
      tempSet.add(kv._1)
      newVertexAttrs(0) = kv._2
      j += 1
    }
     var currSum = indexPair(0).asInstanceOf[Long]
     j = 0
     while ( j < index.size){
         if ( tempSet.contains(currSum)){
             var ind = -1
             var z = 0
             while ( z < auxil.size){
                     if ( auxil(z) == j){
                           ind = z
                          // break
                     }
             }
             // use ind to update vertexAttrs
         }
         currSum = currSum + index(j)
         j += 1
     }
     tot = rt.totalMemory()
     free = rt.freeMemory()
     used = tot - free
     println(s"TOTALVERT: $tot FREEVERT: $free USEDVERT: $used")

    val len = vertexAttrs.length
    println(s"VERTUPS: $j OVERALL: $len")
    val val0 = newVertexAttrs(0)
    println(s"VAL0OFVERT: $val0")
    new EdgePartition( localDstIds1,
localSrcIds, localDstIds, data, index, indexPair,
      auxil,
      newVertexAttrs, activeset)
  }

  /** Return a new `EdgePartition` without any locally cached vertex attributes. */
  def withoutVertexAttributes[VD2: ClassTag](): EdgePartition[ED, VD2] = {
    println(s"WITHOUTANY")
    val newVertexAttrs = new Array[VD2](vertexAttrs.size)
    new EdgePartition( localDstIds1,
localSrcIds, localDstIds, data, index, indexPair,
      auxil, newVertexAttrs,
      activeset)
  }

  @inline private def srcIds(pos: Int): VertexId = {
   println("IN SRCIDS")
  /**  var deBuf = new ByteArrayInputStream(localSrcIds(0))
    val decompressor = new BZip2CompressorInputStream(deBuf)
    var deArr: Array[Byte] = Array.fill[Byte]((indexNext(1) * 4))(0)
   val size4 = decompressor.read(deArr)
   decompressor.close()
   deBuf.close()
   val bb = ByteBuffer.wrap(deArr)
   val localSrcArr: Array[Int] = new Array[Int](indexNext(1))
   var i = 0
   while ( i < indexNext(1) ){
        localSrcArr(i) = bb.getInt(4 * i)
       i += 1
   }
   // println("CHECK")
   // // local2global(localSrcIds(pos)) */
 //   local2global(getSrcIds(0)(pos))
   return indexPair(0)
   }

  @inline private def dstIds(pos: Int): VertexId = {
    println("IN DSTIDS")
/**    var deBuf1 = new ByteArrayInputStream(localDstIds(0))
    val decompressor1 = new BZip2CompressorInputStream(deBuf1)
    var deArr1: Array[Byte] = Array.fill[Byte]((indexNext(1)*4))(0)
   val size5 = decompressor1.read(deArr1)
    decompressor1.close()
    deBuf1.close()
    val bb1 = ByteBuffer.wrap(deArr1)
    val localDstArr: Array[Int] = new Array[Int](indexNext(1))
    var i = 0
    while ( i < indexNext(1) ){
       localDstArr(i) = bb1.getInt(4 * i)
      i += 1
    }
  //  println("CHECK")
   // local2global(localDstIds(pos)) */
    // local2global(getDstIds(0)(pos))
    return indexPair(0)
    }

  @inline private def attrs(pos: Int): ED = getEdgeData()(pos)

  /** Look up vid in activeSet, throwing an exception if it is None. */
  def isActive(j: Int): Boolean = {
//    activeSet.get.contains(vid)
     // activeset.get(j)
      return false
  }

  /** The number of active vertices, if any exist. */
  def numActives: Int = 10

  /**
   * Reverse all the edges in this partition.
   *
   * @return a new edge partition with all edges reversed.
   */
  def reverse: EdgePartition[ED, VD] = {
 //   def reverse: Int ={
val global2local = new GraphXPrimitiveKeyOpenHashMap[VertexId, Int]
    val builder = new ExistingEdgePartitionBuilder[ED, VD](
      global2local, getL2G(), getVertData(), None, size)
    var i = 0
    var dataArr = getEdgeData()
    println("IN REVERSE")
    while (i < size) {
      val localArr = getSrcIds(0)
      val localSrcId = localArr(i)
      val localDstId = localArr(i)
//      val srcId = local2global(localSrcId)
  //    val dstId = local2global(localDstId)
      val srcId = 0
      val dstId = 0
      val attr = dataArr(i)
      builder.add(dstId, srcId, localDstId, localSrcId, attr)
      i += 1
    }
    builder.toEdgePartition
 //   return 0
  }

  /**
   * Construct a new edge partition by applying the function f to all
   * edges in this partition.
   *
   * Be careful not to keep references to the objects passed to `f`.
   * To improve GC performance the same object is re-used for each call.
   *
   * @param f a function from an edge to a new attribute
   * @tparam ED2 the type of the new attribute
   * @return a new edge partition with the result of the function `f`
   *         applied to each edge
   */
  def map[ED2: ClassTag](f: Edge[ED] => ED2): EdgePartition[ED2, VD] = {
    println("IN MAP2")
    val newData = new Array[ED2](data.size)
    val edge = new Edge[ED]()
    val size = data.size
    var dataArr = getEdgeData()
    var i = 0
    while (i < size) {
      edge.srcId = srcIds(i)
      edge.dstId = dstIds(i)
      edge.attr = dataArr(i)
      newData(i) = f(edge)
      i += 1
    }
    this.withData(newData)
  }

  /**
   * Construct a new edge partition by using the edge attributes
   * contained in the iterator.
   *
   * @note The input iterator should return edge attributes in the
   * order of the edges returned by `EdgePartition.iterator` and
   * should return attributes equal to the number of edges.
   *
   * @param iter an iterator for the new attribute values
   * @tparam ED2 the type of the new attribute
   * @return a new edge partition with the attribute values replaced
   */
  def map[ED2: ClassTag](iter: Iterator[ED2]): EdgePartition[ED2, VD] = {
    // Faster than iter.toArray, because the expected size is known.
    println("IN MAP")
    val newData = new Array[ED2](data.size)
    var i = 0
    while (iter.hasNext) {
      newData(i) = iter.next()
      i += 1
    }
    assert(newData.size == i)
    this.withData(newData)
  }
  def getSrcIds(chunk_id: Int): Array[Int] = {
    // return localSrcIds
  /*   val chunk = localSrcIds(chunk_id)
     var deBuf = new ByteArrayInputStream(chunk)
     val decompressor = new SnappyInputStream(deBuf)
     var deArr: Array[Byte] = Array.fill[Byte](indexNext(1) * 4)(0)
     var len = 0
     var total = 0
     while ( len != -1 ) {
       len = decompressor.read(deArr)
       total += len
    }
    val backup = deArr.size
  //  println(s"SRCSIZE: $total  ORIG: $backup")
    decompressor.close()
    deBuf.close()
    val bb = ByteBuffer.wrap(deArr)
    val localSrcArr: Array[Int] = new Array[Int](indexNext(1))
    var i = 0
    while ( i < indexNext(1) ){
       localSrcArr(i) = bb.getInt(4 * i)
       i += 1
      }  */
//     localSrcArr
    return new Array[Int](10)
  }
  def getDstIds(chunk_id: Int): Array[Int] = {
 //    return localDstIds
 /*    val chunk = localDstIds(chunk_id)
     var deBuf = new ByteArrayInputStream(chunk)
     val decompressor = new SnappyInputStream(deBuf)
     var deArr: Array[Byte] = Array.fill[Byte](indexNext(1) * 4)(0)
    var len = 0
    var total = 0
    while (len != -1 ) {
      len = decompressor.read(deArr)
      total += len
    }
    val backup = deArr.size
  //  println(s"DSTSIZE: $total ORIG: $backup")
    decompressor.close()
    deBuf.close()
    val bb1 = ByteBuffer.wrap(deArr)
    val localDstArr: Array[Int] = new Array[Int](indexNext(1))
    var i = 0
    while ( i < indexNext(1) ){
      localDstArr(i) = bb1.getInt(4 * i)
      i += 1
    }
     localDstArr */
     return new Array[Int](10)
  }
  def getVertData(): Array[VD] = {
    vertexAttrs
  }
  def getEdgeData(): Array[ED] = {
    // var debuf = new ByteArrayInputStream(data)
    // val decompressor = new BZip2CompressorInputStream(debuf)
   //  var deArr: Array[Byte] = Array.fill[Byte](indexNext(1) * 10)(0)
   //  var len = 0
   //  var total = 0
   //  while ( len != -1) {
   //    len = decompressor.read(deArr)
   //    total += len
   //  }
   //  val backup = deArr.size
    // println(s"EDGESIZE: $total ORIG: $backup")
   //  decompressor.close()
   // //  debuf.close()
   //  debuf = new ByteArrayInputStream(deArr)
   //  val os = new ObjectInputStream(debuf)
   //  var dataArr: Array[ED] = null
  //  try{
  //    dataArr = os.readObject().asInstanceOf[Array[ED]]
  //  }
  //  catch {
   //    case e: EOFException => {}
   // }
    data
  }
  def getL2G(): Array[VertexId] = {
  //   return local2global
       return indexPair
   /**  var debuf = new ByteArrayInputStream(local2global)
     val decompressor = new BZip2CompressorInputStream(debuf)
     var deArr: Array[Byte] = Array.fill[Byte](indexNext(1) * 8)(0)
    var len = 0
    var total = 0
    while (len != -1) {
      len = decompressor.read(deArr)
      total += len
    }
     val backup = deArr.size
     println(s"L2GSIZE: $total ORIG: $backup")
     decompressor.close()
      debuf.close()
    val bb1 = ByteBuffer.wrap(deArr)
    val l2gArr: Array[VertexId] = new Array[VertexId](indexNext(1))
    var i = 0
    while ( i < indexNext(1) ){
      l2gArr(i) = bb1.getLong(8 * i).asInstanceOf[VertexId]
      i += 1
    }
     l2gArr */

    // debuf = new ByteArrayInputStream(deArr)
    // val os1 = new ObjectInputStream(debuf)
    // var local2globalArr: Array[VertexId] = null
    // try{
     //  local2globalArr = os1.readObject().asInstanceOf[Array[VertexId]]
    // }
    // catch {
    //  case e: EOFException => {}
   //  }
    // local2globalArr
  }
  def compress(): EdgePartition[ED, VD] = {
      var factory = LZ4Factory.fastestInstance
      val compressor = factory.fastCompressor
      val decompressor = factory.fastDecompressor
      val size1 = 10
      val total = 2 * size1 *4
      println(" Starting Compress")
      var time: Long = 0
      var i = 0
      val b = ByteBuffer.allocate(total)
      while ( i < size1 ){
     //     b.putInt(localSrcIds(i))
      //    b.putInt(localDstIds(i))
          i += 1
      }
      var temp = b.array
      val size2 = temp.length
      println(s" SIZE: $size2")
      var buf: Array[Byte] = Array.fill[Byte](total + 10)(0)
      val t1 = System.nanoTime()
      val size3 = compressor.compress(temp, 0, total, buf, 0, total + 10)
      time += (System.nanoTime() - t1)
      println(s" SIZE3: $size3")
      println(s"DONE COMPRESS: $time")
      var deBuf: Array[Byte] = Array.fill[Byte](total)(0)
      val t2 = System.nanoTime()
      val size4 = decompressor.decompress(buf, 0, deBuf, 0, total)
      val time1 = System.nanoTime() - t2
      println(s"SIZE4: $size4")
      println(s"DONE DECOMPRESS: $time1")
      this
  }

  /**
   * Construct a new edge partition containing only the edges matching `epred` and where both
   * vertices match `vpred`.
   */
  def filter(
      epred: EdgeTriplet[VD, ED] => Boolean,
      vpred: (VertexId, VD) => Boolean): EdgePartition[ED, VD] = {
     //  return 0
val global2local = new GraphXPrimitiveKeyOpenHashMap[VertexId, Int]
    val builder = new ExistingEdgePartitionBuilder[ED, VD](
      global2local, getL2G(), getVertData(), None)
    println("IN FILTER")
    var dataArr = getEdgeData()
    var vertArr = getVertData()
    var i = 0
    while (i < 10) {
      // The user sees the EdgeTriplet, so we can't reuse it and must create one per edge.
      val localArr = getSrcIds(0)
      val localSrcId = localArr(i)
      val localDstId = localArr(i)
      val et = new EdgeTriplet[VD, ED]
 //     et.srcId = local2global(localSrcId)
  //    et.dstId = local2global(localDstId)
      et.srcId = 0
      et.dstId = 0
      et.srcAttr = vertArr(localSrcId)
      et.dstAttr = vertArr(localDstId)
      et.attr = dataArr(i)
      if (vpred(et.srcId, et.srcAttr) && vpred(et.dstId, et.dstAttr) && epred(et)) {
        builder.add(et.srcId, et.dstId, localSrcId, localDstId, et.attr)
      }
      i += 1
    }
    builder.toEdgePartition
  }

  /**
   * Apply the function f to all edges in this partition.
   *
   * @param f an external state mutating user defined function.
   */
  def foreach(f: Edge[ED] => Unit) {
    iterator.foreach(f)
  }

  /**
   * Merge all the edges with the same src and dest id into a single
   * edge using the `merge` function
   *
   * @param merge a commutative associative merge operation
   * @return a new edge partition without duplicate edges
   */
  def groupEdges(merge: (ED, ED) => ED): EdgePartition[ED, VD] = {
 //  return 0
 val global2local = new GraphXPrimitiveKeyOpenHashMap[VertexId, Int]
    val builder = new ExistingEdgePartitionBuilder[ED, VD](
      global2local, getL2G(), getVertData(), None)
    var currSrcId: VertexId = null.asInstanceOf[VertexId]
    var currDstId: VertexId = null.asInstanceOf[VertexId]
    var currLocalSrcId = -1
    var currLocalDstId = -1
    var currAttr: ED = null.asInstanceOf[ED]
    // Iterate through the edges, accumulating runs of identical edges using the curr* variables and
    // releasing them to the builder when we see the beginning of the next run
    println("GROUP EDGES")
    var dataArr = getEdgeData()
    var i = 0
    while (i < 10) {
      if (i > 0 && currSrcId == srcIds(i) && currDstId == dstIds(i)) {
        // This edge should be accumulated into the existing run
        currAttr = merge(currAttr, dataArr(i))
      } else {
        // This edge starts a new run of edges
        if (i > 0) {
          // First release the existing run to the builder
          builder.add(currSrcId, currDstId, currLocalSrcId, currLocalDstId, currAttr)
        }
        // Then start accumulating for a new run
        currSrcId = srcIds(i)
        currDstId = dstIds(i)
        val localArr = getSrcIds(0)
        currLocalSrcId = localArr(i)
        currLocalDstId = localArr(i)
        currAttr = dataArr(i)
      }
      i += 1
    }
    // Finally, release the last accumulated run
    if (size > 0) {
      builder.add(currSrcId, currDstId, currLocalSrcId, currLocalDstId, currAttr)
    }
    builder.toEdgePartition
  }

  /**
   * Apply `f` to all edges present in both `this` and `other` and return a new `EdgePartition`
   * containing the resulting edges.
   *
   * If there are multiple edges with the same src and dst in `this`, `f` will be invoked once for
   * each edge, but each time it may be invoked on any corresponding edge in `other`.
   *
   * If there are multiple edges with the same src and dst in `other`, `f` will only be invoked
   * once.
   */
  def innerJoin[ED2: ClassTag, ED3: ClassTag]
      (other: EdgePartition[ED2, _])
      (f: (VertexId, VertexId, ED, ED2) => ED3): EdgePartition[ED3, VD] = {
 //   return 0
val global2local = new GraphXPrimitiveKeyOpenHashMap[VertexId, Int]

    val builder = new ExistingEdgePartitionBuilder[ED3, VD](
      global2local, getL2G(), getVertData(), None)
    println("INNER JOIN")
    var i = 0
    var j = 0
    // For i = index of each edge in `this`...
    while (i < size && j < other.size) {
      val srcId = this.srcIds(i)
      val dstId = this.dstIds(i)
      // ... forward j to the index of the corresponding edge in `other`, and...
      while (j < other.size && other.srcIds(j) < srcId) { j += 1 }
      if (j < other.size && other.srcIds(j) == srcId) {
        while (j < other.size && other.srcIds(j) == srcId && other.dstIds(j) < dstId) { j += 1 }
        if (j < other.size && other.srcIds(j) == srcId && other.dstIds(j) == dstId) {
          // ... run `f` on the matching edge
          var temp = getEdgeData()
          builder.add(srcId, dstId, getSrcIds(0)(i), getSrcIds(0)(i),
            f(srcId, dstId, temp(i), other.attrs(j)))
        }
      }
      i += 1
    }
    builder.toEdgePartition
  }

  /**
   * The number of edges in this partition
   *
   * @return size of the partition
   */
  val size: Int = 10

  /** The number of unique source vertices in the partition. */
  def indexSize: Int = index.size

  /**
   * Get an iterator over the edges in this partition.
   *
   * Be careful not to keep references to the objects from this iterator.
   * To improve GC performance the same object is re-used in `next()`.
   *
   * @return an iterator over edges in the partition
   */
  def iterator: Iterator[Edge[ED]] = new Iterator[Edge[ED]] {
    private[this] val edge = new Edge[ED]
    private[this] var pos = 0
    private[this] var localSrcArr: Array[Int] = localDstIds1
    private[this] var localDstArr: Array[Int] = localDstIds1
    private[this] var dataArr: Array[ED] = null
    private[this] var local2globalArr: Array[VertexId] = null

    override def hasNext: Boolean = pos < 10

    override def next(): Edge[ED] = {
      if ( pos == 0 ) {
         getSrcIds(0)
        getDstIds(0)
        dataArr = getEdgeData()
        local2globalArr = getL2G()
      }
      edge.srcId = local2globalArr(localSrcArr(pos))
      edge.dstId = local2globalArr(localDstArr(pos))
      edge.attr = dataArr(pos)
      pos += 1
      edge
    }
  }

  /**
   * Get an iterator over the edge triplets in this partition.
   *
   * It is safe to keep references to the objects from this iterator.
   */
  def tripletIterator(
      includeSrc: Boolean = true, includeDst: Boolean = true)
      : Iterator[EdgeTriplet[VD, ED]] = new Iterator[EdgeTriplet[VD, ED]] {
    private[this] var pos = 0
    private[this] var chunk_id = -1
    private[this] var localSrcArr: Array[Int] = localDstIds1
    private[this] var localDstArr: Array[Int] = localDstIds1
    private[this] var dataArr: Array[ED] = getEdgeData()
    private[this] var local2globalArr: Array[VertexId] = getL2G()
    private[this] var vertexAttrArr: Array[VD] = null


    println("TRIPLET ITERATOR")
    override def hasNext: Boolean = pos < 10

    override def next(): EdgeTriplet[VD, ED] = {
     if ( pos == 11 ) {
     chunk_id += 1
     getSrcIds(chunk_id)
     getDstIds(chunk_id)
  //   dataArr = getEdgeData()
//     local2globalArr = getL2G()
    }
    if ( pos == 0 && (includeSrc || includeDst)){
     vertexAttrArr = getVertData()
    }
      val triplet = new EdgeTriplet[VD, ED]
      val localSrcId = localSrcArr(pos)
      val localDstId = localDstArr(pos)
      triplet.srcId = local2globalArr(localSrcId)
      triplet.dstId = local2globalArr(localDstId)
      if (includeSrc) {
        triplet.srcAttr = vertexAttrArr(localSrcId)
      }
      if (includeDst) {
        triplet.dstAttr = vertexAttrArr(localDstId)
      }
      triplet.attr = dataArr(pos)
      pos += 1
      triplet
    }
  }

  /**
   * Send messages along edges and aggregate them at the receiving vertices. Implemented by scanning
   * all edges sequentially.
   *
   * @param sendMsg generates messages to neighboring vertices of an edge
   * @param mergeMsg the combiner applied to messages destined to the same vertex
   * @param tripletFields which triplet fields `sendMsg` uses
   * @param activeness criteria for filtering edges based on activeness
   *
   * @return iterator aggregated messages keyed by the receiving vertex id
   */
  def aggregateMessagesEdgeScan[A: ClassTag](
      sendMsg: EdgeContext[VD, ED, A] => Unit,
      mergeMsg: (A, A) => A,
      tripletFields: TripletFields,
      activeness: EdgeActiveness): Iterator[(VertexId, A)] = {
     println("IN EDGE SCAN")
     val rt = Runtime.getRuntime()
     var tot = rt.totalMemory()
     var free = rt.freeMemory()
     var used = tot - free
     println(s"TOTAL: $tot FREE: $free USED: $used")
     var localSrcArr: Array[Int] = localDstIds1
     var localDstArr: Array[Int] = localDstIds1
     getSrcIds(0)
     getDstIds(0)
     var dataArr: Array[ED] = getEdgeData()
     var local2globalArr: Array[VertexId] = getL2G()
     var vertexAttrArr: Array[VD] = getVertData()
     tot = rt.totalMemory()
     free = rt.freeMemory()
     used = tot - free
     var chunk_id = 0
     println(s"TOTAL: $tot FREE: $free USED: $used")
    val aggregates = new Array[A](vertexAttrs.size)
    val bitset = new BitSet(vertexAttrs.size)
    var ctx = new AggregatingEdgeContext[VD, ED, A](mergeMsg, aggregates, bitset)
    var i = 0
    // val srcSize = localSrcArr.size
    // val dstSize = localDstArr.size
    val arrSize1 = 10
    var sumSrc = 0
    var sumDst = 0
    var localSrcId = 0
    // println(s"SIZE5: $srcSize and SIZE4: $dstSize and ORIGSIZE: $arrSize1")
    while (i < arrSize1) {
      if (false){
        localSrcId = 1 + sumSrc
      }
      val localDstId = sumDst + 1
      val srcActive = auxil(localSrcId)
      val dstActive = auxil(localDstId)
      val edgeIsActive =
        if (activeness == EdgeActiveness.Neither) true
        else if (activeness == EdgeActiveness.SrcOnly) isActive(srcActive)
        else if (activeness == EdgeActiveness.DstOnly) isActive(dstActive)
    else if (activeness == EdgeActiveness.Both) isActive(srcActive) && isActive(dstActive)
    else if (activeness == EdgeActiveness.Either) isActive(srcActive) || isActive(dstActive)
        else throw new Exception("unreachable")
      if (edgeIsActive) {
        val srcAttr = if (tripletFields.useSrc) vertexAttrArr(localSrcId) else null.asInstanceOf[VD]
        val dstAttr = if (tripletFields.useDst) vertexAttrArr(localDstId) else null.asInstanceOf[VD]
        ctx.set(0, 0, localSrcId, localDstId, srcAttr, dstAttr, dataArr(i))
        sendMsg(ctx)
      }
      i += 1
    }

    bitset.iterator.map { localId => (local2globalArr(localId), aggregates(localId)) }
  }

  /**
   * Send messages along edges and aggregate them at the receiving vertices. Implemented by
   * filtering the source vertex index, then scanning each edge cluster.
   *
   * @param sendMsg generates messages to neighboring vertices of an edge
   * @param mergeMsg the combiner applied to messages destined to the same vertex
   * @param tripletFields which triplet fields `sendMsg` uses
   * @param activeness criteria for filtering edges based on activeness
   *
   * @return iterator aggregated messages keyed by the receiving vertex id
   */
  def aggregateMessagesIndexScan[A: ClassTag](
      sendMsg: EdgeContext[VD, ED, A] => Unit,
      mergeMsg: (A, A) => A,
      tripletFields: TripletFields,
      activeness: EdgeActiveness): Iterator[(VertexId, A)] = {
   println("IN INDEX SCAN")
     val rt = Runtime.getRuntime()
     var tot = rt.totalMemory()
     var free = rt.freeMemory()
     var used = tot - free
     println(s"TOTAL: $tot FREE: $free USED: $used")
     var localSrcArr: Array[Int] = localDstIds1
     var localDstArr: Array[Int] = localDstIds1
     getSrcIds(0)
     getDstIds(0)
     var dataArr: Array[ED] = getEdgeData()
     var local2globalArr: Array[VertexId] = getL2G()
     var vertexAttrArr: Array[VD] = getVertData()
    tot = rt.totalMemory()
    free = rt.freeMemory()
    used = tot - free
    println(s"TOTAL: $tot FREE: $free USED: $used")
    var skipped = 0
    var edgeSkipped = 0
    var totalSkipped = 0
    var totalEdgeSkipped = 0
    var chunk_id = 0
    val aggregates = new Array[A](vertexAttrs.size)
    val bitset = new BitSet(vertexAttrs.size)
    var ctx = new AggregatingEdgeContext[VD, ED, A](mergeMsg, aggregates, bitset)
 //   index.iterator.foreach { cluster =>
  //    val clusterSrcId = cluster._1
   //   val clusterPos = cluster._2
      // val clusterLocalSrcId = localSrcArr(clusterPos)
      var cluster = 0
      while(cluster < index.size) {
      val clusterSrcId = index(cluster)
      val clusterPos = indexPair(cluster)
      val scanCluster =
        if (activeness == EdgeActiveness.Neither) true
        else if (activeness == EdgeActiveness.SrcOnly) isActive(clusterSrcId)
        else if (activeness == EdgeActiveness.DstOnly) true
        else if (activeness == EdgeActiveness.Both) isActive(clusterSrcId)
        else if (activeness == EdgeActiveness.Either) true
        else throw new Exception("unreachable")
      totalSkipped += 1
      if (!scanCluster) skipped += 1
      if (scanCluster) {
        var pos = clusterPos
        var temp = pos
         var chunk_idNew = 0
  //       while (pos >= indexNext(chunk_idNew + 1)) {
   //            chunk_idNew += 1
     //    }
     /**    if(chunk_idNew != chunk_id){
             chunk_id = chunk_idNew
             getDstIds(chunk_id)
             getSrcIds(chunk_id)
     //        println(s"DECOMP $chunk_id")
          } */
     // val clusterLocalSrcId = localSrcArr(clusterPos)
        val clusterLocalSrcId = 0
    //  println(s"POS: $pos and CHUNKID $chunk_id")

        val srcAttr =
          if (tripletFields.useSrc) vertexAttrArr(clusterLocalSrcId) else null.asInstanceOf[VD]
        ctx.setSrcOnly(clusterSrcId, clusterLocalSrcId, srcAttr)
        while (pos < 10 && 0 == clusterLocalSrcId) {
         // val localDstId = localDstArr(pos)
          val localDstId = 0
          val dstId = local2globalArr(localDstId)
          val edgeIsActive =
            if (activeness == EdgeActiveness.Neither) true
            else if (activeness == EdgeActiveness.SrcOnly) true
            else if (activeness == EdgeActiveness.DstOnly) isActive(0)
            else if (activeness == EdgeActiveness.Both) isActive(0)
            else if (activeness == EdgeActiveness.Either) isActive(2) || isActive(1)
            else throw new Exception("unreachable")
          if (!edgeIsActive) edgeSkipped += 1
          totalEdgeSkipped += 1
          if (edgeIsActive) {
            val dstAttr =
              if (tripletFields.useDst) vertexAttrArr(localDstId) else null.asInstanceOf[VD]
            ctx.setRest(dstId, localDstId, dstAttr, dataArr(0))
            sendMsg(ctx)
          }
          pos += 1
 //       if ( pos == indexNext(chunk_id + 1) && chunk_id < 3){
   //        chunk_id += 1
     //       getSrcIds(chunk_id)
       //    getDstIds(chunk_id)
       //    println(s"2DECOMP $chunk_id")
     //   }

        }
      }
      cluster += 1
    }
    println(s"CLUSTERSKIPPED: $skipped TOTALCLUSTER: $totalSkipped")
    println(s"EDGESKIPPED: $edgeSkipped TOTALEDGE: $totalEdgeSkipped")
    bitset.iterator.map { localId => (local2globalArr(localId), aggregates(localId)) }
      }
}

private class AggregatingEdgeContext[VD, ED, A](
    mergeMsg: (A, A) => A,
    aggregates: Array[A],
    bitset: BitSet)
  extends EdgeContext[VD, ED, A] {

  private[this] var _srcId: VertexId = _
  private[this] var _dstId: VertexId = _
  private[this] var _localSrcId: Int = _
  private[this] var _localDstId: Int = _
  private[this] var _srcAttr: VD = _
  private[this] var _dstAttr: VD = _
  private[this] var _attr: ED = _

  def set(
      srcId: VertexId, dstId: VertexId,
      localSrcId: Int, localDstId: Int,
      srcAttr: VD, dstAttr: VD,
      attr: ED) {

    _srcId = srcId
    _dstId = dstId
    _localSrcId = localSrcId
    _localDstId = localDstId
    _srcAttr = srcAttr
    _dstAttr = dstAttr
    _attr = attr
  }

  def setSrcOnly(srcId: VertexId, localSrcId: Int, srcAttr: VD) {
    _srcId = srcId
    _localSrcId = localSrcId
    _srcAttr = srcAttr
  }

  def setRest(dstId: VertexId, localDstId: Int, dstAttr: VD, attr: ED) {
    _dstId = dstId
    _localDstId = localDstId
    _dstAttr = dstAttr
    _attr = attr
  }

  override def srcId: VertexId = _srcId
  override def dstId: VertexId = _dstId
  override def srcAttr: VD = _srcAttr
  override def dstAttr: VD = _dstAttr
  override def attr: ED = _attr

  override def sendToSrc(msg: A) {
    send(_localSrcId, msg)
  }
  override def sendToDst(msg: A) {
    send(_localDstId, msg)
  }

  @inline private def send(localId: Int, msg: A) {
    if (bitset.get(localId)) {
      aggregates(localId) = mergeMsg(aggregates(localId), msg)
    } else {
      aggregates(localId) = msg
      bitset.set(localId)
    }
  }
}
// scalastyle:on println
