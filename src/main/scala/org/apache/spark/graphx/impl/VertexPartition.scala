/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.spark.graphx.impl

import scala.reflect.ClassTag

import org.apache.spark.util.collection.BitSet
import java.io.ByteArrayOutputStream
import java.io.ByteArrayInputStream
import java.io.DataOutputStream
import java.io.DataInputStream
import java.nio.ByteBuffer
import java.io.ObjectOutputStream
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream

import org.apache.spark.graphx._
import org.apache.spark.graphx.util.collection.GraphXPrimitiveKeyOpenHashMap

private[graphx] object VertexPartition {
  /** Construct a `VertexPartition` from the given vertices. */
  def apply[VD: ClassTag](iter: Iterator[(VertexId, VD)])
    : VertexPartition[VD] = {
    val (index, values, mask) = VertexPartitionBase.initFrom(iter)
    var z = 0
  /**  var z = -1
    values(0) match {
      case Int => z = 0
      case Double => z = 1
      case Long => z = 2
     }
    var bb = ByteBuffer.allocate(values.size * 8)
    var i = 0
      while ( i < values.size ){
        bb.putDouble(values(i).asInstanceOf[Double])
        i += 1
      }
   var buf = new ByteArrayOutputStream()
   val compressor = new BZip2CompressorOutputStream(buf)
   val arr = bb.array()
   compressor.write(arr, 0, arr.size)
   compressor.flush()
   compressor.close()
   val valComp = buf.toByteArray() */
   val values1 = new Array[Any](values.size)
   z = 0
   while ( z < values.size ) {
      values1(z) = values(z)
   }
   new VertexPartition(index, values, values1, mask, z)
  }

  import scala.language.implicitConversions

  /**
   * Implicit conversion to allow invoking `VertexPartitionBase` operations directly on a
   * `VertexPartition`.
   */
  implicit def partitionToOps[VD: ClassTag](partition: VertexPartition[VD])
    : VertexPartitionOps[VD] = new VertexPartitionOps(partition)

  /**
   * Implicit evidence that `VertexPartition` is a member of the `VertexPartitionBaseOpsConstructor`
   * typeclass. This enables invoking `VertexPartitionBase` operations on a `VertexPartition` via an
   * evidence parameter, as in [[VertexPartitionBaseOps]].
   */
  implicit object VertexPartitionOpsConstructor
    extends VertexPartitionBaseOpsConstructor[VertexPartition] {
    def toOps[VD: ClassTag](partition: VertexPartition[VD])
      : VertexPartitionBaseOps[VD, VertexPartition] = partitionToOps(partition)
  }
}

/** A map from vertex id to vertex attribute. */
private[graphx] class VertexPartition[VD: ClassTag](
    val index: VertexIdToIndexMap,
    val values: Array[VD],
    val values1: Array[Any],
    val mask: BitSet,
    val typecheck: Integer)
  extends VertexPartitionBase[VD]

private[graphx] class VertexPartitionOps[VD: ClassTag](self: VertexPartition[VD])
  extends VertexPartitionBaseOps[VD, VertexPartition](self) {

  def withIndex(index: VertexIdToIndexMap): VertexPartition[VD] = {
    new VertexPartition(index, self.values, self.values1, self.mask, self.typecheck)
  }

  def withValues1(values1: Array[Any]): VertexPartition[VD] = {
    new VertexPartition(self.index, self.values, values1, self.mask, self.typecheck)
  }

  def withValues[VD2: ClassTag](values: Array[VD2]): VertexPartition[VD2] = {
    new VertexPartition(self.index, values, self.values1, self.mask, self.typecheck)
  }

  def withMask(mask: BitSet): VertexPartition[VD] = {
    new VertexPartition(self.index, self.values, self.values1, mask, self.typecheck)
  }
}
