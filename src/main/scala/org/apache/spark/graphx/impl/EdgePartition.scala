/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.spark.graphx.impl
import net.jpountz.lz4._
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream
import java.io.ByteArrayOutputStream
import java.io.ByteArrayInputStream
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.EOFException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import scala.reflect.{classTag, ClassTag}
import java.nio.ByteBuffer
import org.apache.spark.graphx._
import org.apache.spark.graphx.util.collection.GraphXPrimitiveKeyOpenHashMap
import org.apache.spark.util.collection.BitSet
// import java.util.BitSet
// scalastyle:off println
/**
 * A collection of edges, along with referenced vertex attributes and an optional active vertex set
 * for filtering computation on the edges.
 *
 * The edges are stored in columnar format in `localSrcIds`, `localDstIds`, and `data`. All
 * referenced global vertex ids are mapped to a compact set of local vertex ids according to the
 * `global2local` map. Each local vertex id is a valid index into `vertexAttrs`, which stores the
 * corresponding vertex attribute, and `local2global`, which stores the reverse mapping to global
 * vertex id. The global vertex ids that are active are optionally stored in `activeSet`.
 *
 * The edges are clustered by source vertex id, and the mapping from global vertex id to the index
 * of the corresponding edge cluster is stored in `index`.
 *
 * @tparam ED the edge attribute type
 * @tparam VD the vertex attribute type
 *
 * @param localSrcIds the local source vertex id of each edge as an index into `local2global` and
 *   `vertexAttrs`
 * @param localDstIds the local destination vertex id of each edge as an index into `local2global`
 *   and `vertexAttrs`
 * @param data the attribute associated with each edge
 * @param index a clustered index on source vertex id as a map from each global source vertex id to
 *   the offset in the edge arrays where the cluster for that vertex id begins
 * @param global2local a map from referenced vertex ids to local ids which index into vertexAttrs
 * @param local2global an array of global vertex ids where the offsets are local vertex ids
 * @param vertexAttrs an array of vertex attributes where the offsets are local vertex ids
 * @param activeSet an optional active vertex set for filtering computation on the edges
 */
private[graphx]
class EdgePartition[
    @specialized(Char, Int, Boolean, Byte, Long, Float, Double) ED: ClassTag, VD: ClassTag](
    localSrcIds: Array[Int],
    localDstIds: Array[Int],
    data: Array[ED],
    index: GraphXPrimitiveKeyOpenHashMap[VertexId, Int],
    global2local: GraphXPrimitiveKeyOpenHashMap[VertexId, Int],
    local2global: Array[VertexId],
    vertexAttrs: Array[VD],
    activeSet: Option[VertexSet],
    activeBitSet: Option[BitSet],
    localDstIndex: Array[Int],
    localDstDelta: Array[Byte],
    localSrcBitSet: BitSet,
    vertIdIndex: Array[VertexId],
    vertIdDelta: Array[Byte],
    l2gMap: Array[Int],
    g2lMap: Array[Int])
  extends Serializable {
  var accumDe: Long = 0
  /** No-arg constructor for serialization. */
  private def this() = this(null, null, null, null, null, null, null, null,
  null, null, null, null, null, null, null, null)

  /** Return a new `EdgePartition` with the specified edge data. */
  def withData[ED2: ClassTag](data: Array[ED2]): EdgePartition[ED2, VD] = {
    println("WITHDATA")
   // val ba = new ByteArrayOutputStream()
   // val os = new ObjectOutputStream(ba)
   // os.writeObject(data)
   // os.flush()
  //  os.close()
  //  ba.flush()
  //  ba.close()
  //  val dataArr = ba.toByteArray()
  //  var buf3 = new ByteArrayOutputStream()
  //  val compressor2 = new BZip2CompressorOutputStream(buf3)
  // /a/  compressor2.write(dataArr)
  //  compressor2.flush()
  //  buf3.flush()
   // compressor2.close()
  //  buf3.close()
  //  val dataArrCompressed = buf3.toByteArray()
   // println(s"IN WITH DATA")
    new EdgePartition(
      localSrcIds, localDstIds, data, index, global2local,
      local2global, vertexAttrs, activeSet, activeBitSet, localDstIndex, localDstDelta,
      localSrcBitSet, vertIdIndex, vertIdDelta, l2gMap, g2lMap)
  }

  /** Return a new `EdgePartition` with the specified active set, provided as an iterator. */
  def withActiveSet(iter: Iterator[VertexId]): EdgePartition[ED, VD] = {
    println(s"WITHACTIVE")
    val activeSet = new VertexSet
    var j = 0
    var arr = new Array[Long](local2global.size)
    val t1 = System.nanoTime()
    while (iter.hasNext) {
       val temp = iter.next()
       activeSet.add(temp)
       arr(j) = temp.asInstanceOf[Long]
       j += 1 }
    println(s"VERTSETSIZE: $j")
    val time = System.nanoTime() - t1
    var arr1 = new Array[Long](j)
    val t2 = System.nanoTime()
    System.arraycopy(arr1, 0, arr, 0, j)
    scala.util.Sorting.quickSort(arr1)
    var currSum: Long = vertIdIndex(0)
    var curr = 0
    var k = 0
    var flag = false
    val activeBitset = new org.apache.spark.util.collection.BitSet(arr1.size)
    while( k < arr1.size && j < vertIdDelta.size) {
      if (flag){
 //       currSum = currSum + vertIdDelta(j)
          currSum = currSum + 1
      }
    if ( currSum == arr1(k)){
         if ( j >= 0 && j < arr1.size){
         activeBitset.set(j)
         }
         k += 1
         j += 1
         flag = true
    }
    else if ( currSum < arr1(k)){
         j += 1
         flag = true
    }
    else {
         k += 1
         flag = false
    }
    }
    val time2 = System.nanoTime() - t2
    println(s"ACTIVETIME O(V): $time2")
    new EdgePartition(
      localSrcIds, localDstIds, data, index, global2local, local2global, vertexAttrs,
      Some(activeSet), activeBitSet, localDstIndex, localDstDelta, localSrcBitSet, vertIdIndex,
      vertIdDelta, l2gMap, g2lMap)
  }

  /** Return a new `EdgePartition` with updates to vertex attributes specified in `iter`. */
  def updateVertices(iter: Iterator[(VertexId, VD)]): EdgePartition[ED, VD] = {
    var newVertexAttrs = new Array[VD](vertexAttrs.size)
    System.arraycopy(vertexAttrs, 0, newVertexAttrs, 0, vertexAttrs.size)
    var j = 0
//    val tempSet = new VertexSet
    val arr = new Array[VertexId](vertexAttrs.size)
    while (iter.hasNext) {
      val kv = iter.next()
      val t1 = System.nanoTime()
     // tempSet.add(kv._1)
      arr(j) = kv._1
      newVertexAttrs(global2local(kv._1)) = kv._2
      j += 1
    }
   var k = 0
     var curr = 0
    var currSum: Long = vertIdIndex(0)
    var flag = false
    val t2 = System.nanoTime()
    while( k < arr.size && j < vertIdDelta.size) {
      if (flag){
 //       currSum = currSum + vertIdDelta(j)
          currSum = currSum + 1
      }
    if ( currSum == arr(k)){
         if ( j >= 0 && j < g2lMap.size){
         val ind = g2lMap(j)
          }
         // set vertexAttrs(ind) do arr(k).data
         curr += 1
         j += 1
         flag = true
    }
    else if ( currSum < arr(k)){
         j += 1
         flag = true
    }
    else {
         k += 1
         flag = false
    }
    }
    val time2 = System.nanoTime() - t2
    val check = j
    val len = vertexAttrs.length
    println(s"VERTUPS: $check OVERALL: $len")
    val val0 = newVertexAttrs(0)
    println(s"VAL0OFVERT: $val0")
    println(s"VertDataUpdate: O(V) $time2")
    new EdgePartition(
      localSrcIds, localDstIds, data, index, global2local, local2global,
      newVertexAttrs, activeSet, activeBitSet, localDstIndex, localDstDelta, localSrcBitSet,
      vertIdIndex, vertIdDelta, l2gMap, g2lMap)
  }

  /** Return a new `EdgePartition` without any locally cached vertex attributes. */
  def withoutVertexAttributes[VD2: ClassTag](): EdgePartition[ED, VD2] = {
    println(s"WITHOUTANY")
    val newVertexAttrs = new Array[VD2](vertexAttrs.size)
    new EdgePartition(
      localSrcIds, localDstIds, data, index, global2local, local2global, newVertexAttrs,
      activeSet, activeBitSet, localDstIndex, localDstDelta, localSrcBitSet,
      vertIdIndex, vertIdDelta, l2gMap, g2lMap)
  }

  @inline private def srcIds(pos: Int): VertexId = {
   // var deBuf = new ByteArrayInputStream(localSrcIds)
   // val decompressor = new BZip2CompressorInputStream(deBuf)
   // var deArr: Array[Byte] = Array.fill[Byte]((arrSize(0) * 4))(0)
    // val size4 = decompressor.decompress(buf, 0, deBuf, 0, total)
   // val size4 = decompressor.read(deArr)
   // decompressor.close()
   // deBuf.close()
   // val bb = ByteBuffer.wrap(deArr)
   // val localSrcArr: Array[Int] = new Array[Int](arrSize(0))
   // var i = 0
   // while ( i < arrSize(0) ){
   //     localSrcArr(i) = bb.getInt(4 * i)
  //      i += 1
   // }
   // println("CHECK")
   // // local2global(localSrcIds(pos))
    local2global(localSrcIds(pos))
   }

  @inline private def dstIds(pos: Int): VertexId = {
   // var deBuf1 = new ByteArrayInputStream(localDstIds)
   // val decompressor1 = new BZip2CompressorInputStream(deBuf1)
  //  var deArr1: Array[Byte] = Array.fill[Byte]((arrSize(0)*4))(0)
   //  val size4 = decompressor.decompress(buf, 0, deBuf, 0, total)
  //  val size5 = decompressor1.read(deArr1)
  //  decompressor1.close()
  //  deBuf1.close()
  //  val bb1 = ByteBuffer.wrap(deArr1)
  //  val localDstArr: Array[Int] = new Array[Int](arrSize(0))
  //  var i = 0
  //  while ( i < arrSize(0) ){
  //     localDstArr(i) = bb1.getInt(4 * i)
  //     i += 1
  //  }
  //  println("CHECK")
    local2global(localDstIds(pos))
   //  local2global(localDstIds(pos))
    }

  @inline private def attrs(pos: Int): ED = getEdgeData()(pos)

  /** Look up vid in activeSet, throwing an exception if it is None. */
  def isActive(vid: VertexId): Boolean = {
    activeSet.get.contains(vid)
  }

  /** The number of active vertices, if any exist. */
  def numActives: Option[Int] = activeSet.map(_.size)

  /**
   * Reverse all the edges in this partition.
   *
   * @return a new edge partition with all edges reversed.
   */
  def reverse: EdgePartition[ED, VD] = {
    val builder = new ExistingEdgePartitionBuilder[ED, VD](
      global2local, getL2G(), getVertData(), activeSet, size)
    var i = 0
    var dataArr = getEdgeData()
    println("IN REVERSE")
    while (i < size) {
      val localSrcId = localSrcIds(i)
      val localDstId = localDstIds(i)
      val srcId = local2global(localSrcId)
      val dstId = local2global(localDstId)
      val attr = dataArr(i)
      builder.add(dstId, srcId, localDstId, localSrcId, attr)
      i += 1
    }
    builder.toEdgePartition
  }
  def findUsingRLEIndex(index: Int): Int = {
      val gapSize = 6
      val chunk: Int = index / gapSize
      val maxIter = gapSize / 2
      var currSum: Long = 0
      if ( chunk >= 0 && chunk < vertIdIndex.size){
         currSum = vertIdIndex(chunk)
      }
      var j = index
      while ( j < maxIter && j < vertIdDelta.size) {
 //         currSum = currSum + vertIdDelta(j)
          currSum = currSum + 1
          j += 1
      }
      return j
     }
  /**
   * Construct a new edge partition by applying the function f to all
   * edges in this partition.
   *
   * Be careful not to keep references to the objects passed to `f`.
   * To improve GC performance the same object is re-used for each call.
   *
   * @param f a function from an edge to a new attribute
   * @tparam ED2 the type of the new attribute
   * @return a new edge partition with the result of the function `f`
   *         applied to each edge
   */
  def map[ED2: ClassTag](f: Edge[ED] => ED2): EdgePartition[ED2, VD] = {
    println("IN MAP2")
    val newData = new Array[ED2](data.size)
    val edge = new Edge[ED]()
    val size = data.size
    var dataArr = getEdgeData()
    var i = 0
    var j = 0
    var currSumSrc = 0
    var currSumDst = 0
    while (i < size) {
      edge.srcId = srcIds(i)
      // bit set should represent diff between i+1 and i
      if ( localSrcBitSet.get(i) == true){
           currSumSrc += 1
      }
      if (currSumSrc >= l2gMap.size) currSumSrc = 0
      val gloSrc = l2gMap(currSumSrc)
      if ( j >= localDstDelta.size) {
          j = 0
          currSumDst = 0
      }
      if (currSumDst >= l2gMap.size) currSumDst = 0
      val gloDst = l2gMap(currSumDst)
      val globalIdSrc = findUsingRLEIndex(gloSrc)
      val globalIdDst = findUsingRLEIndex(gloDst)
      currSumDst += localDstDelta(j)
      edge.dstId = dstIds(i)
      edge.attr = dataArr(i)
      newData(i) = f(edge)
      i += 1
      j += 1
    }
    this.withData(newData)
  }

  /**
   * Construct a new edge partition by using the edge attributes
   * contained in the iterator.
   *
   * @note The input iterator should return edge attributes in the
   * order of the edges returned by `EdgePartition.iterator` and
   * should return attributes equal to the number of edges.
   *
   * @param iter an iterator for the new attribute values
   * @tparam ED2 the type of the new attribute
   * @return a new edge partition with the attribute values replaced
   */
  def map[ED2: ClassTag](iter: Iterator[ED2]): EdgePartition[ED2, VD] = {
    // Faster than iter.toArray, because the expected size is known.
    println("IN MAP")
    val newData = new Array[ED2](data.size)
    var i = 0
    while (iter.hasNext) {
      newData(i) = iter.next()
      i += 1
    }
    assert(newData.size == i)
    this.withData(newData)
  }
  def getSrcIds(): Array[Int] = {
    //  var deBuf = new ByteArrayInputStream(localSrcIds)
    //  val decompressor = new BZip2CompressorInputStream(deBuf)
    //  var deArr: Array[Byte] = Array.fill[Byte](arrSize(0) * 4)(0)
    // val size4 = decompressor.decompress(buf, 0, deBuf, 0, total)
    //  var len = 0
    //  var total = 0
    //  while ( len != -1 ) {
    //    len = decompressor.read(deArr)
    //    total += len
    // }
    // val backup = deArr.size
    // println(s"SRCSIZE: $total  ORIG: $backup")
    // decompressor.close()
    // deBuf.close()
    // val bb = ByteBuffer.wrap(deArr)
    // val localSrcArr: Array[Int] = new Array[Int](arrSize(0))
    // var i = 0
    // while ( i < arrSize(0) ){
    //    localSrcArr(i) = bb.getInt(4 * i)
    //    i += 1
    // }
     localSrcIds
  }
  def getDstIds(): Array[Int] = {
   //  var deBuf = new ByteArrayInputStream(localDstIds)
   //  val decompressor = new BZip2CompressorInputStream(deBuf)
   //  var deArr: Array[Byte] = Array.fill[Byte](arrSize(0) * 4)(0)
    // val size4 = decompressor.decompress(buf, 0, deBuf, 0, total)
   //  var len = 0
   //  var total = 0
   //  while (len != -1 ) {
   //    len = decompressor.read(deArr)
   //    total += len
   //  }
   //  val backup = deArr.size
   //  println(s"DSTSIZE: $total ORIG: $backup")
   //  decompressor.close()
   //  deBuf.close()
   //  val bb1 = ByteBuffer.wrap(deArr)
   //  val localDstArr: Array[Int] = new Array[Int](arrSize(0))
   //  var i = 0
   //  while ( i < arrSize(0) ){
    //   localDstArr(i) = bb1.getInt(4 * i)
    //   i += 1
    // }
     localDstIds
  }
  def getVertData(): Array[VD] = {
    vertexAttrs
  }
  def getEdgeData(): Array[ED] = {
    // var debuf = new ByteArrayInputStream(data)
    // val decompressor = new BZip2CompressorInputStream(debuf)
   //  var deArr: Array[Byte] = Array.fill[Byte](arrSize(0) * 10)(0)
   //  var len = 0
   //  var total = 0
   //  while ( len != -1) {
   //    len = decompressor.read(deArr)
   //    total += len
   //  }
   //  val backup = deArr.size
    // println(s"EDGESIZE: $total ORIG: $backup")
   //  decompressor.close()
   // //  debuf.close()
   //  debuf = new ByteArrayInputStream(deArr)
   //  val os = new ObjectInputStream(debuf)
   //  var dataArr: Array[ED] = null
  //  try{
  //    dataArr = os.readObject().asInstanceOf[Array[ED]]
  //  }
  //  catch {
   //    case e: EOFException => {}
   // }
    data
  }
  def getL2G(): Array[VertexId] = {
    local2global
    // var debuf = new ByteArrayInputStream(local2global)
    // val decompressor = new BZip2CompressorInputStream(debuf)
    // var deArr: Array[Byte] = Array.fill[Byte](arrSize(0) * 8)(0)
    // var len = 0
    // var total = 0
    // while (len != -1) {
    //   len = decompressor.read(deArr)
    //   total += len
   //  }
    // val backup = deArr.size
    // println(s"L2GSIZE: $total ORIG: $backup")
    // decompressor.close()
    // debuf.close()
    // debuf = new ByteArrayInputStream(deArr)
    // val os1 = new ObjectInputStream(debuf)
    // var local2globalArr: Array[VertexId] = null
    // try{
     //  local2globalArr = os1.readObject().asInstanceOf[Array[VertexId]]
    // }
    // catch {
    //  case e: EOFException => {}
   //  }
    // local2globalArr
  }
  def compress(): EdgePartition[ED, VD] = {
      var factory = LZ4Factory.fastestInstance
      val compressor = factory.fastCompressor
      val decompressor = factory.fastDecompressor
      val size1 = localSrcIds.size
      val total = 2 * size1 *4
      println(" Starting Compress")
      var time: Long = 0
      var i = 0
      val b = ByteBuffer.allocate(total)
      while ( i < size1 ){
          b.putInt(localSrcIds(i))
          b.putInt(localDstIds(i))
          i += 1
      }
      var temp = b.array
      val size2 = temp.length
      println(s" SIZE: $size2")
      var buf: Array[Byte] = Array.fill[Byte](total + 10)(0)
      val t1 = System.nanoTime()
      val size3 = compressor.compress(temp, 0, total, buf, 0, total + 10)
      time += (System.nanoTime() - t1)
      println(s" SIZE3: $size3")
      println(s"DONE COMPRESS: $time")
      var deBuf: Array[Byte] = Array.fill[Byte](total)(0)
      val t2 = System.nanoTime()
      val size4 = decompressor.decompress(buf, 0, deBuf, 0, total)
      val time1 = System.nanoTime() - t2
      println(s"SIZE4: $size4")
      println(s"DONE DECOMPRESS: $time1")
      this
  }

  /**
   * Construct a new edge partition containing only the edges matching `epred` and where both
   * vertices match `vpred`.
   */
  def filter(
      epred: EdgeTriplet[VD, ED] => Boolean,
      vpred: (VertexId, VD) => Boolean): EdgePartition[ED, VD] = {
    val builder = new ExistingEdgePartitionBuilder[ED, VD](
      global2local, getL2G(), getVertData(), activeSet)
    println("IN FILTER")
    var dataArr = getEdgeData()
    var vertArr = getVertData()
    var i = 0
    while (i < localSrcIds.size) {
      // The user sees the EdgeTriplet, so we can't reuse it and must create one per edge.
      val localSrcId = localSrcIds(i)
      val localDstId = localDstIds(i)
      val et = new EdgeTriplet[VD, ED]
      et.srcId = local2global(localSrcId)
      et.dstId = local2global(localDstId)
      et.srcAttr = vertArr(localSrcId)
      et.dstAttr = vertArr(localDstId)
      et.attr = dataArr(i)
      if (vpred(et.srcId, et.srcAttr) && vpred(et.dstId, et.dstAttr) && epred(et)) {
        builder.add(et.srcId, et.dstId, localSrcId, localDstId, et.attr)
      }
      i += 1
    }
    builder.toEdgePartition
  }

  /**
   * Apply the function f to all edges in this partition.
   *
   * @param f an external state mutating user defined function.
   */
  def foreach(f: Edge[ED] => Unit) {
    iterator.foreach(f)
  }

  /**
   * Merge all the edges with the same src and dest id into a single
   * edge using the `merge` function
   *
   * @param merge a commutative associative merge operation
   * @return a new edge partition without duplicate edges
   */
  def groupEdges(merge: (ED, ED) => ED): EdgePartition[ED, VD] = {
    val builder = new ExistingEdgePartitionBuilder[ED, VD](
      global2local, getL2G(), getVertData(), activeSet)
    var currSrcId: VertexId = null.asInstanceOf[VertexId]
    var currDstId: VertexId = null.asInstanceOf[VertexId]
    var currLocalSrcId = -1
    var currLocalDstId = -1
    var currAttr: ED = null.asInstanceOf[ED]
    // Iterate through the edges, accumulating runs of identical edges using the curr* variables and
    // releasing them to the builder when we see the beginning of the next run
    println("GROUP EDGES")
    var dataArr = getEdgeData()
    var i = 0
    while (i < localSrcIds.size) {
      if (i > 0 && currSrcId == srcIds(i) && currDstId == dstIds(i)) {
        // This edge should be accumulated into the existing run
        currAttr = merge(currAttr, dataArr(i))
      } else {
        // This edge starts a new run of edges
        if (i > 0) {
          // First release the existing run to the builder
          builder.add(currSrcId, currDstId, currLocalSrcId, currLocalDstId, currAttr)
        }
        // Then start accumulating for a new run
        currSrcId = srcIds(i)
        currDstId = dstIds(i)
        currLocalSrcId = localSrcIds(i)
        currLocalDstId = localDstIds(i)
        currAttr = dataArr(i)
      }
      i += 1
    }
    // Finally, release the last accumulated run
    if (size > 0) {
      builder.add(currSrcId, currDstId, currLocalSrcId, currLocalDstId, currAttr)
    }
    builder.toEdgePartition
  }

  /**
   * Apply `f` to all edges present in both `this` and `other` and return a new `EdgePartition`
   * containing the resulting edges.
   *
   * If there are multiple edges with the same src and dst in `this`, `f` will be invoked once for
   * each edge, but each time it may be invoked on any corresponding edge in `other`.
   *
   * If there are multiple edges with the same src and dst in `other`, `f` will only be invoked
   * once.
   */
  def innerJoin[ED2: ClassTag, ED3: ClassTag]
      (other: EdgePartition[ED2, _])
      (f: (VertexId, VertexId, ED, ED2) => ED3): EdgePartition[ED3, VD] = {
    val builder = new ExistingEdgePartitionBuilder[ED3, VD](
      global2local, getL2G(), getVertData(), activeSet)
    println("INNER JOIN")
    var i = 0
    var j = 0
    // For i = index of each edge in `this`...
    while (i < size && j < other.size) {
      val srcId = this.srcIds(i)
      val dstId = this.dstIds(i)
      // ... forward j to the index of the corresponding edge in `other`, and...
      while (j < other.size && other.srcIds(j) < srcId) { j += 1 }
      if (j < other.size && other.srcIds(j) == srcId) {
        while (j < other.size && other.srcIds(j) == srcId && other.dstIds(j) < dstId) { j += 1 }
        if (j < other.size && other.srcIds(j) == srcId && other.dstIds(j) == dstId) {
          // ... run `f` on the matching edge
          var temp = getEdgeData()
          builder.add(srcId, dstId, localSrcIds(i), localDstIds(i),
            f(srcId, dstId, temp(i), other.attrs(j)))
        }
      }
      i += 1
    }
    builder.toEdgePartition
  }

  /**
   * The number of edges in this partition
   *
   * @return size of the partition
   */
  val size: Int = localSrcIds.size

  /** The number of unique source vertices in the partition. */
  def indexSize: Int = index.size

  /**
   * Get an iterator over the edges in this partition.
   *
   * Be careful not to keep references to the objects from this iterator.
   * To improve GC performance the same object is re-used in `next()`.
   *
   * @return an iterator over edges in the partition
   */
  def iterator: Iterator[Edge[ED]] = new Iterator[Edge[ED]] {
    private[this] val edge = new Edge[ED]
    private[this] var pos = 0
    private[this] var localSrcArr: Array[Int] = null
    private[this] var localDstArr: Array[Int] = null
    private[this] var dataArr: Array[ED] = null
    private[this] var local2globalArr: Array[VertexId] = null

    override def hasNext: Boolean = pos < localSrcIds.size

    override def next(): Edge[ED] = {
      if ( pos == 0 ) {
        localSrcArr = getSrcIds()
        localDstArr = getDstIds()
        dataArr = getEdgeData()
        local2globalArr = getL2G()
      }
      edge.srcId = local2globalArr(localSrcArr(pos))
      edge.dstId = local2globalArr(localDstArr(pos))
      edge.attr = dataArr(pos)
      pos += 1
      edge
    }
  }

  /**
   * Get an iterator over the edge triplets in this partition.
   *
   * It is safe to keep references to the objects from this iterator.
   */
  def tripletIterator(
      includeSrc: Boolean = true, includeDst: Boolean = true)
      : Iterator[EdgeTriplet[VD, ED]] = new Iterator[EdgeTriplet[VD, ED]] {
    private[this] var pos = 0
    private[this] var currSumSrc = 0
    private[this] var currSumDst = 0


    private[this] var j = 0
    private[this] var localSrcArr: Array[Int] = null
    private[this] var localDstArr: Array[Int] = null
    private[this] var dataArr: Array[ED] = null
    private[this] var local2globalArr: Array[VertexId] = null
    private[this] var vertexAttrArr: Array[VD] = null


    println("TRIPLET ITERATOR")
    override def hasNext: Boolean = pos < localSrcIds.size

    override def next(): EdgeTriplet[VD, ED] = {
     if ( pos == 0 ) {
     localSrcArr = getSrcIds()
     localDstArr = getDstIds()
     dataArr = getEdgeData()
     local2globalArr = getL2G()
    }
    if ( pos == 0 && (includeSrc || includeDst)){
     vertexAttrArr = getVertData()
    }
     if ( localSrcBitSet.get(pos) == true){
           currSumSrc += 1
      }
      if ( currSumSrc >= l2gMap.size) currSumSrc = 0
      val gloSrc = l2gMap(currSumSrc)
      if ( pos >= localDstDelta.size) {
          j = 0
          currSumDst = 0
      }

//      currSumDst += localDstDelta(j)
      currSumDst += 1
      if ( currSumDst >= l2gMap.size) currSumDst = 0
      val gloDst = l2gMap(currSumDst)
      val globalIdSrc = findUsingRLEIndex(gloSrc)
      val globalIdDst = findUsingRLEIndex(gloDst)

      val triplet = new EdgeTriplet[VD, ED]
      val localSrcId = localSrcArr(pos)
      val localDstId = localDstArr(pos)
      triplet.srcId = local2globalArr(localSrcId)
      triplet.dstId = local2globalArr(localDstId)
      if (includeSrc) {
        triplet.srcAttr = vertexAttrArr(localSrcId)
      }
      if (includeDst) {
        triplet.dstAttr = vertexAttrArr(localDstId)
      }
      triplet.attr = dataArr(pos)
      pos += 1
      triplet
    }
  }

  /**
   * Send messages along edges and aggregate them at the receiving vertices. Implemented by scanning
   * all edges sequentially.
   *
   * @param sendMsg generates messages to neighboring vertices of an edge
   * @param mergeMsg the combiner applied to messages destined to the same vertex
   * @param tripletFields which triplet fields `sendMsg` uses
   * @param activeness criteria for filtering edges based on activeness
   *
   * @return iterator aggregated messages keyed by the receiving vertex id
   */
  def aggregateMessagesEdgeScan[A: ClassTag](
      sendMsg: EdgeContext[VD, ED, A] => Unit,
      mergeMsg: (A, A) => A,
      tripletFields: TripletFields,
      activeness: EdgeActiveness): Iterator[(VertexId, A)] = {
     println("IN EDGE SCAN")
     val rt = Runtime.getRuntime()
     var tot = rt.totalMemory()
     var free = rt.freeMemory()
     var used = tot - free
     println(s"TOTAL: $tot FREE: $free USED: $used")
     val localSrcArr: Array[Int] = getSrcIds()
     val localDstArr: Array[Int] = getDstIds()
     var dataArr: Array[ED] = getEdgeData()
     var local2globalArr: Array[VertexId] = getL2G()
     var vertexAttrArr: Array[VD] = getVertData()
     tot = rt.totalMemory()
     free = rt.freeMemory()
     used = tot - free
     println(s"TOTAL: $tot FREE: $free USED: $used")
    val aggregates = new Array[A](vertexAttrs.size)
    val bitset = new BitSet(vertexAttrs.size)
    var ctx = new AggregatingEdgeContext[VD, ED, A](mergeMsg, aggregates, bitset)
    var i = 0
    // val srcSize = localSrcArr.size
    // val dstSize = localDstArr.size
    val arrSize1 = localSrcIds.size
    // println(s"SIZE5: $srcSize and SIZE4: $dstSize and ORIGSIZE: $arrSize1")
    var currSumSrc = 0
    var currSumDst = 0
    var dstCount = 0
    while (i < arrSize1) {
    if ( localSrcBitSet.get(0) == true) {
            currSumSrc += 1
     }
     if ( dstCount >= localDstDelta.size) {
          dstCount = 0
          currSumDst = 0
     }
 //    currSumDst += localDstDelta.asInstanceOf[Int]
     currSumDst += 1
     if ( currSumSrc >= l2gMap.size) currSumSrc = 0
     var ind = l2gMap(currSumSrc)
    // for some reason i get doesn't take params
 //    val flag = activeBitSet.get(ind)
     if ( currSumDst >= l2gMap.size) currSumDst = 0

     ind = l2gMap(currSumDst)
  //   val dstFlag = activeBitSet.get(ind)

     var ji = 0
      var sum = 0
   //   println("DONE1")
      val localSrcId = localSrcArr(i)
      val srcId = local2global(localSrcId)
      val localDstId = localDstArr(i)
      val dstId = local2global(localDstId)
      val edgeIsActive =
        if (activeness == EdgeActiveness.Neither) true
        else if (activeness == EdgeActiveness.SrcOnly) isActive(srcId)
        else if (activeness == EdgeActiveness.DstOnly) isActive(dstId)
        else if (activeness == EdgeActiveness.Both) isActive(srcId) && isActive(dstId)
        else if (activeness == EdgeActiveness.Either) isActive(srcId) || isActive(dstId)
        else throw new Exception("unreachable")
      if (edgeIsActive) {
       val gloSrc = findUsingRLEIndex(l2gMap(currSumSrc))
      val gloDst = findUsingRLEIndex(l2gMap(currSumDst))
        val srcAttr = if (tripletFields.useSrc) vertexAttrArr(localSrcId) else null.asInstanceOf[VD]
        val dstAttr = if (tripletFields.useDst) vertexAttrArr(localDstId) else null.asInstanceOf[VD]
        ctx.set(srcId, dstId, localSrcId, localDstId, srcAttr, dstAttr, dataArr(i))
        sendMsg(ctx)
      }
      i += 1
    }

    bitset.iterator.map { localId => (local2globalArr(localId), aggregates(localId)) }
  }

  /**
   * Send messages along edges and aggregate them at the receiving vertices. Implemented by
   * filtering the source vertex index, then scanning each edge cluster.
   *
   * @param sendMsg generates messages to neighboring vertices of an edge
   * @param mergeMsg the combiner applied to messages destined to the same vertex
   * @param tripletFields which triplet fields `sendMsg` uses
   * @param activeness criteria for filtering edges based on activeness
   *
   * @return iterator aggregated messages keyed by the receiving vertex id
   */
  def aggregateMessagesIndexScan[A: ClassTag](
      sendMsg: EdgeContext[VD, ED, A] => Unit,
      mergeMsg: (A, A) => A,
      tripletFields: TripletFields,
      activeness: EdgeActiveness): Iterator[(VertexId, A)] = {
   println("IN INDEX SCAN")
     val rt = Runtime.getRuntime()
     var tot = rt.totalMemory()
     var free = rt.freeMemory()
     var used = tot - free
     println(s"TOTAL: $tot FREE: $free USED: $used")
     val localSrcArr: Array[Int] = getSrcIds()
     val localDstArr: Array[Int] = getDstIds()
     var dataArr: Array[ED] = getEdgeData()
     var local2globalArr: Array[VertexId] = getL2G()
     var vertexAttrArr: Array[VD] = getVertData()
    tot = rt.totalMemory()
    free = rt.freeMemory()
    used = tot - free
    println(s"TOTAL: $tot FREE: $free USED: $used")
    var skipped = 0
    var edgeSkipped = 0
    var totalSkipped = 0
    var totalEdgeSkipped = 0
    val aggregates = new Array[A](vertexAttrs.size)
    val bitset = new BitSet(vertexAttrs.size)
    var ctx = new AggregatingEdgeContext[VD, ED, A](mergeMsg, aggregates, bitset)
    index.iterator.foreach { cluster =>
      val clusterSrcId = cluster._1
      val clusterPos = cluster._2
      val clusterLocalSrcId = localSrcArr(clusterPos)
      // get particular source value as local then check if active using bitset
      val scanCluster =
        if (activeness == EdgeActiveness.Neither) true
        else if (activeness == EdgeActiveness.SrcOnly) isActive(clusterSrcId)
        else if (activeness == EdgeActiveness.DstOnly) true
        else if (activeness == EdgeActiveness.Both) isActive(clusterSrcId)
        else if (activeness == EdgeActiveness.Either) true
        else throw new Exception("unreachable")
      totalSkipped += 1
      if (!scanCluster) skipped += 1
      if (scanCluster) {
        var pos = clusterPos
        val srcAttr =
          if (tripletFields.useSrc) vertexAttrArr(clusterLocalSrcId) else null.asInstanceOf[VD]
        ctx.setSrcOnly(clusterSrcId, clusterLocalSrcId, srcAttr)
        while (pos < localSrcIds.size && localSrcArr(pos) == clusterLocalSrcId) {
          val localDstId = localDstArr(pos)
          val dstId = local2globalArr(localDstId)
          val edgeIsActive =
            if (activeness == EdgeActiveness.Neither) true
            else if (activeness == EdgeActiveness.SrcOnly) true
            else if (activeness == EdgeActiveness.DstOnly) isActive(dstId)
            else if (activeness == EdgeActiveness.Both) isActive(dstId)
            else if (activeness == EdgeActiveness.Either) isActive(clusterSrcId) || isActive(dstId)
            else throw new Exception("unreachable")
          if (!edgeIsActive) edgeSkipped += 1
          totalEdgeSkipped += 1
          if (edgeIsActive) {
            findUsingRLEIndex(13)
            findUsingRLEIndex(27)
            val dstAttr =
              if (tripletFields.useDst) vertexAttrArr(localDstId) else null.asInstanceOf[VD]
            ctx.setRest(dstId, localDstId, dstAttr, dataArr(pos))
            sendMsg(ctx)
          }
          pos += 1
        }
      }
    }
    println(s"CLUSTERSKIPPED: $skipped TOTALCLUSTER: $totalSkipped")
    println(s"EDGESKIPPED: $edgeSkipped TOTALEDGE: $totalEdgeSkipped")
    bitset.iterator.map { localId => (local2globalArr(localId), aggregates(localId)) }
      }
}

private class AggregatingEdgeContext[VD, ED, A](
    mergeMsg: (A, A) => A,
    aggregates: Array[A],
    bitset: BitSet)
  extends EdgeContext[VD, ED, A] {

  private[this] var _srcId: VertexId = _
  private[this] var _dstId: VertexId = _
  private[this] var _localSrcId: Int = _
  private[this] var _localDstId: Int = _
  private[this] var _srcAttr: VD = _
  private[this] var _dstAttr: VD = _
  private[this] var _attr: ED = _

  def set(
      srcId: VertexId, dstId: VertexId,
      localSrcId: Int, localDstId: Int,
      srcAttr: VD, dstAttr: VD,
      attr: ED) {

    _srcId = srcId
    _dstId = dstId
    _localSrcId = localSrcId
    _localDstId = localDstId
    _srcAttr = srcAttr
    _dstAttr = dstAttr
    _attr = attr
  }

  def setSrcOnly(srcId: VertexId, localSrcId: Int, srcAttr: VD) {
    _srcId = srcId
    _localSrcId = localSrcId
    _srcAttr = srcAttr
  }

  def setRest(dstId: VertexId, localDstId: Int, dstAttr: VD, attr: ED) {
    _dstId = dstId
    _localDstId = localDstId
    _dstAttr = dstAttr
    _attr = attr
  }

  override def srcId: VertexId = _srcId
  override def dstId: VertexId = _dstId
  override def srcAttr: VD = _srcAttr
  override def dstAttr: VD = _dstAttr
  override def attr: ED = _attr

  override def sendToSrc(msg: A) {
    send(_localSrcId, msg)
  }
  override def sendToDst(msg: A) {
    send(_localDstId, msg)
  }

  @inline private def send(localId: Int, msg: A) {
    if (bitset.get(localId)) {
      aggregates(localId) = mergeMsg(aggregates(localId), msg)
    } else {
      aggregates(localId) = msg
      bitset.set(localId)
    }
  }
}
// scalastyle:on println
